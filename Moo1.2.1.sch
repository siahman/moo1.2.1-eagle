<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<description>Moo 1.2 Antenna Front End</description>
<libraries>
<library name="Moo1.2">
<packages>
<package name="UDFN6_1PRIMARY">
<description>Original name &lt;b&gt;UDFN6&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.4064" y="-0.4826" dx="0.2286" dy="0.7112" layer="1" rot="R180"/>
<smd name="2" x="0" y="-0.4826" dx="0.2286" dy="0.7112" layer="1" rot="R180"/>
<smd name="3" x="0.4064" y="-0.4826" dx="0.2286" dy="0.7112" layer="1" rot="R180"/>
<smd name="4" x="0.4064" y="0.4826" dx="0.2286" dy="0.7112" layer="1" rot="R180"/>
<smd name="5" x="0" y="0.4826" dx="0.2286" dy="0.7112" layer="1" rot="R180"/>
<smd name="6" x="-0.4064" y="0.4826" dx="0.2286" dy="0.7112" layer="1" rot="R180"/>
<circle x="-0.7112" y="-0.6858" radius="0.0508" width="0.1" layer="21"/>
<wire x1="-0.6096" y1="-0.508" x2="-0.6096" y2="0.508" width="0.1" layer="21"/>
<wire x1="0.6096" y1="-0.508" x2="0.6096" y2="0.508" width="0.1" layer="21"/>
<text x="-0.8103875" y="1.18300625" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.8103875" y="0.92900625" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SC82-AB_1PRIMARY">
<description>Original name &lt;b&gt;SC82-AB&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1.3208" y="-0.8128" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="2" x="-0.0254" y="-0.8128" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="3" x="-0.0254" y="1.0668" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="4" x="-1.3208" y="1.0668" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<wire x1="-1.5748" y1="-0.2794" x2="0.2286" y2="-0.2794" width="0.2" layer="21"/>
<wire x1="-1.5748" y1="-0.2794" x2="-1.5748" y2="0.5334" width="0.2" layer="21"/>
<wire x1="-1.5748" y1="0.5334" x2="0.2286" y2="0.5334" width="0.2" layer="21"/>
<wire x1="0.2286" y1="-0.2794" x2="0.2286" y2="0.5334" width="0.2" layer="21"/>
<text x="-1.984121875" y="1.89260625" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.984121875" y="1.63860625" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOT-23-5_1PRIMARY">
<description>Original name &lt;b&gt;SOT-23-5&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1.1938" y="0.9398" dx="0.6096" dy="1.0922" layer="1" rot="R90"/>
<smd name="2" x="-1.1938" y="0" dx="0.6096" dy="1.0922" layer="1" rot="R90"/>
<smd name="3" x="-1.1938" y="-0.9398" dx="0.6096" dy="1.0922" layer="1" rot="R90"/>
<smd name="4" x="1.1938" y="-0.9398" dx="0.6096" dy="1.0922" layer="1" rot="R90"/>
<smd name="5" x="1.1938" y="0.9398" dx="0.6096" dy="1.0922" layer="1" rot="R90"/>
<wire x1="0.8636" y1="-1.4986" x2="0.8636" y2="1.4986" width="0.1" layer="21"/>
<wire x1="-0.8636" y1="1.4986" x2="0.8636" y2="1.4986" width="0.1" layer="21"/>
<wire x1="-0.8636" y1="-1.4986" x2="0.8636" y2="-1.4986" width="0.1" layer="21"/>
<wire x1="-0.8636" y1="-1.4986" x2="-0.8636" y2="1.4986" width="0.1" layer="21"/>
<text x="-1.851609375" y="1.804009375" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.851609375" y="1.550009375" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOIC8_1PRIMARY">
<description>Original name &lt;b&gt;SOIC8&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-2.7178" y="1.9304" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<smd name="2" x="-2.7178" y="0.6604" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<smd name="3" x="-2.7178" y="-0.635" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<smd name="4" x="-2.7178" y="-1.905" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<smd name="5" x="2.667" y="-1.905" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<smd name="6" x="2.667" y="-0.635" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<smd name="7" x="2.667" y="0.6604" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<smd name="8" x="2.667" y="1.9304" dx="0.6096" dy="2.2098" layer="1" rot="R270"/>
<wire x1="-0.3801625" y1="2.46395" x2="0.378078125" y2="2.442109375" width="0.2" layer="21" curve="169.100278"/>
<circle x="-2.794" y="2.7178" radius="0.127" width="0.2499875" layer="21"/>
<wire x1="-1.1938" y1="2.4892" x2="-0.4318" y2="2.4892" width="0.2" layer="21"/>
<wire x1="1.2192" y1="-2.4638" x2="1.2192" y2="2.4892" width="0.2" layer="21"/>
<wire x1="0.4318" y1="2.4892" x2="1.2192" y2="2.4892" width="0.2" layer="21"/>
<wire x1="-1.143" y1="-2.4892" x2="1.2192" y2="-2.4892" width="0.2" layer="21"/>
<wire x1="-1.2192" y1="-2.4892" x2="1.2192" y2="-2.4892" width="0.2" layer="21"/>
<wire x1="-1.2192" y1="-2.4892" x2="-1.2192" y2="2.4892" width="0.2" layer="21"/>
<text x="-3.910203125" y="3.2218125" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-3.910203125" y="2.9678125" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SC70-5_1PRIMARY">
<description>Original name &lt;b&gt;sc70-5&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.6604" y="-0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="2" x="0" y="-0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="3" x="0.6604" y="-0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="4" x="0.6604" y="0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="5" x="-0.6604" y="0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<wire x1="-0.889" y1="-0.4064" x2="-0.889" y2="0.4064" width="0.2" layer="21"/>
<wire x1="-0.889" y1="-0.4064" x2="0.889" y2="-0.4064" width="0.2" layer="21"/>
<wire x1="-0.889" y1="0.4064" x2="0.889" y2="0.4064" width="0.2" layer="21"/>
<wire x1="0.889" y1="-0.4064" x2="0.889" y2="0.4064" width="0.2" layer="21"/>
<text x="-0.999996875" y="1.76560625" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.999996875" y="1.51160625" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOT723_1PRIMARY">
<description>Original name &lt;b&gt;SOT723&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.508" y="0.4064" dx="0.4064" dy="0.4064" layer="1" rot="R90"/>
<smd name="2" x="-0.508" y="-0.4064" dx="0.4064" dy="0.4064" layer="1" rot="R90"/>
<smd name="3" x="0.508" y="0" dx="0.4064" dy="0.4064" layer="1" rot="R90"/>
<wire x1="0.2286" y1="-0.6096" x2="0.4318" y2="-0.6096" width="0.2" layer="21"/>
<wire x1="0.2286" y1="0.6096" x2="0.4318" y2="0.6096" width="0.2" layer="21"/>
<text x="-0.8016" y="0.9556" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.8016" y="0.7016" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOD-523_1PRIMARY">
<description>Original name &lt;b&gt;SOD-523&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="0" y="0" dx="0.4064" dy="0.4064" layer="1"/>
<smd name="2" x="0" y="-1.397" dx="0.4064" dy="0.4064" layer="1"/>
<wire x1="-0.4064" y1="-0.5334" x2="-0.4064" y2="-0.254" width="0.1" layer="21"/>
<wire x1="-0.4064" y1="-0.254" x2="0.4064" y2="-0.254" width="0.1" layer="21"/>
<wire x1="0.4064" y1="-0.5334" x2="0.4064" y2="-0.254" width="0.1" layer="21"/>
<text x="-0.45130625" y="0.5556" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.45130625" y="0.3016" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="0603_-_RES_1PRIMARY">
<description>Original name &lt;b&gt;0603_-_RES&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.8382" y="0" dx="0.9906" dy="1.0922" layer="1" rot="R90"/>
<smd name="2" x="0.8382" y="0" dx="0.9906" dy="1.0922" layer="1" rot="R90"/>
<wire x1="-1.5494" y1="0.635" x2="1.5494" y2="0.635" width="0.0762" layer="21"/>
<wire x1="1.5494" y1="-0.635" x2="1.5494" y2="0.635" width="0.0762" layer="21"/>
<wire x1="-1.5494" y1="-0.635" x2="-1.5494" y2="0.635" width="0.0762" layer="21"/>
<wire x1="-1.524" y1="-0.635" x2="1.524" y2="-0.635" width="0.0762" layer="21"/>
<text x="-1.5748" y="0.9271" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.5748" y="0.6731" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="0805_1PRIMARY">
<description>Original name &lt;b&gt;0805&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.9398" y="0" dx="1.4986" dy="1.2954" layer="1" rot="R90"/>
<smd name="2" x="0.9398" y="0" dx="1.4986" dy="1.2954" layer="1" rot="R90"/>
<wire x1="-1.905" y1="-0.6604" x2="-1.905" y2="0.6604" width="0.2" layer="21"/>
<text x="-1.999996875" y="1.1056125" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.999996875" y="0.8516125" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="FC-135_1PRIMARY">
<description>Original name &lt;b&gt;FC-135&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1.2446" y="0" dx="0.9906" dy="1.8034" layer="1"/>
<smd name="2" x="1.2446" y="0" dx="0.9906" dy="1.8034" layer="1"/>
<wire x1="-0.762" y1="0.762" x2="0.762" y2="0.762" width="0.254" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="0.762" y2="-0.762" width="0.254" layer="21"/>
<text x="-1.851609375" y="1.255596875" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.851609375" y="1.001596875" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="0402_1PRIMARY">
<description>Original name &lt;b&gt;0402&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.6604" y="0" dx="0.7112" dy="0.889" layer="1" rot="R90"/>
<smd name="2" x="0.6604" y="0" dx="0.7112" dy="0.889" layer="1" rot="R90"/>
<wire x1="-1.2446" y1="-0.4826" x2="-1.2446" y2="0.4826" width="0.0762" layer="21"/>
<wire x1="-1.2446" y1="-0.4826" x2="1.2446" y2="-0.4826" width="0.0762" layer="21"/>
<wire x1="-1.2446" y1="0.4826" x2="1.2446" y2="0.4826" width="0.0762" layer="21"/>
<wire x1="1.2446" y1="-0.4826" x2="1.2446" y2="0.4826" width="0.0762" layer="21"/>
<text x="-1.27" y="0.7747" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.27" y="0.5207" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOT-323,_HSMS-285C_1PRIMARY">
<description>Original name &lt;b&gt;SOT-323,_HSMS-285C&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.6604" y="-1.016" dx="0.7112" dy="0.9906" layer="1"/>
<smd name="2" x="0.6604" y="-1.016" dx="0.7112" dy="0.9906" layer="1"/>
<smd name="3" x="0" y="1.016" dx="0.7112" dy="0.9906" layer="1"/>
<wire x1="-1.0668" y1="-0.6604" x2="-1.0668" y2="0.6604" width="0.1" layer="21"/>
<wire x1="-1.0668" y1="-0.6604" x2="1.0668" y2="-0.6604" width="0.1" layer="21"/>
<wire x1="-1.0668" y1="0.6604" x2="1.0668" y2="0.6604" width="0.1" layer="21"/>
<wire x1="1.0668" y1="-0.6604" x2="1.0668" y2="0.6604" width="0.1" layer="21"/>
<text x="-1.124990625" y="1.880590625" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.124990625" y="1.626590625" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="0603_1PRIMARY">
<description>Original name &lt;b&gt;0603&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.8382" y="0" dx="0.9906" dy="1.0922" layer="1" rot="R90"/>
<smd name="2" x="0.8382" y="0" dx="0.9906" dy="1.0922" layer="1" rot="R90"/>
<wire x1="-1.7018" y1="-0.4064" x2="-1.7018" y2="0.4064" width="0.2" layer="21"/>
<text x="-1.799996875" y="0.8556" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.799996875" y="0.6016" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="VAR-CAP_603_1PRIMARY">
<description>Original name &lt;b&gt;VAR-CAP_603&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-1.4478" y="0" dx="0.9906" dy="1.0922" layer="1" rot="R90"/>
<smd name="2" x="0.8382" y="0" dx="2.3114" dy="0.9906" layer="1"/>
<wire x1="1.6002" y1="-1.2446" x2="1.6002" y2="1.2446" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="1.2446" x2="1.6002" y2="1.2446" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-1.2446" x2="1.6002" y2="-1.2446" width="0.127" layer="21"/>
<wire x1="-1.6002" y1="0.5334" x2="-0.889" y2="1.2446" width="0.127" layer="21"/>
<wire x1="-1.6002" y1="-0.5334" x2="-1.6002" y2="0.5334" width="0.127" layer="21"/>
<wire x1="-1.6002" y1="-0.5334" x2="-0.889" y2="-1.2446" width="0.127" layer="21"/>
<text x="-1.999996875" y="1.567509375" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.999996875" y="1.313509375" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOT-23_1PRIMARY">
<description>Original name &lt;b&gt;SOT-23&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.9398" y="-1.1938" dx="0.8382" dy="0.9906" layer="1"/>
<smd name="2" x="0.9398" y="-1.1938" dx="0.8382" dy="0.9906" layer="1"/>
<smd name="3" x="0" y="1.1938" dx="0.8382" dy="0.9906" layer="1"/>
<wire x1="-1.27" y1="0.5842" x2="1.27" y2="0.5842" width="0.2" layer="21"/>
<wire x1="1.27" y1="-0.5842" x2="1.27" y2="0.5842" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-0.5842" x2="1.27" y2="-0.5842" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-0.5842" x2="-1.27" y2="0.5842" width="0.2" layer="21"/>
<text x="-1.476603125" y="1.953996875" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.476603125" y="1.699996875" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="PROBE_POINT_2PRIMARY">
<description>Original name &lt;b&gt;PROBE_POINT&lt;/b&gt;&lt;p&gt;</description>
<pad name="1" x="0" y="0" drill="0.508" diameter="0.762"/>
<text x="-0.4826" y="0.7366" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.4826" y="0.4826" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="MHDR1X10_1PRIMARY">
<description>Original name &lt;b&gt;MHDR1X10&lt;/b&gt;&lt;p&gt;</description>
<pad name="1" x="0" y="0" drill="0.6096" diameter="0.9906" shape="square"/>
<pad name="2" x="1.27" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="3" x="2.54" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="4" x="3.81" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="5" x="5.08" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="6" x="6.35" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="7" x="7.62" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="8" x="8.89" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="9" x="10.16" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="10" x="11.43" y="0" drill="0.6096" diameter="0.9906"/>
<wire x1="-0.889" y1="-0.889" x2="-0.889" y2="0.889" width="0.2" layer="21"/>
<wire x1="-0.889" y1="-0.889" x2="12.319" y2="-0.889" width="0.2" layer="21"/>
<wire x1="-0.889" y1="0.889" x2="12.319" y2="0.889" width="0.2" layer="21"/>
<wire x1="12.319" y1="-0.889" x2="12.319" y2="0.889" width="0.2" layer="21"/>
<text x="-0.999996875" y="1.253996875" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.999996875" y="0.999996875" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="MHDR1X14_1PRIMARY">
<description>Original name &lt;b&gt;MHDR1X14&lt;/b&gt;&lt;p&gt;</description>
<pad name="1" x="0" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="2" x="1.27" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="3" x="2.54" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="4" x="3.81" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="5" x="5.08" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="6" x="6.35" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="7" x="7.62" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="8" x="8.89" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="9" x="10.16" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="10" x="11.43" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="11" x="12.7" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="12" x="13.97" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="13" x="15.24" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="14" x="16.51" y="0" drill="0.6096" diameter="0.9906" shape="square"/>
<wire x1="-0.889" y1="-0.889" x2="-0.889" y2="0.889" width="0.2" layer="21"/>
<wire x1="-0.889" y1="-0.889" x2="17.399" y2="-0.889" width="0.2" layer="21"/>
<wire x1="-0.889" y1="0.889" x2="17.399" y2="0.889" width="0.2" layer="21"/>
<wire x1="17.399" y1="-0.889" x2="17.399" y2="0.889" width="0.2" layer="21"/>
<text x="-0.999996875" y="1.253996875" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.999996875" y="0.999996875" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="MHDR1X4_1PRIMARY">
<description>Original name &lt;b&gt;MHDR1X4&lt;/b&gt;&lt;p&gt;</description>
<pad name="1" x="0" y="0" drill="0.6096" diameter="0.9906" shape="square"/>
<pad name="2" x="1.27" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="3" x="2.54" y="0" drill="0.6096" diameter="0.9906"/>
<pad name="4" x="3.81" y="0" drill="0.6096" diameter="0.9906"/>
<wire x1="-0.889" y1="-0.889" x2="4.699" y2="-0.889" width="0.2" layer="21"/>
<wire x1="-0.889" y1="-0.889" x2="-0.889" y2="0.889" width="0.2" layer="21"/>
<wire x1="-0.889" y1="0.889" x2="4.699" y2="0.889" width="0.2" layer="21"/>
<wire x1="4.699" y1="-0.889" x2="4.699" y2="0.889" width="0.2" layer="21"/>
<text x="-0.999996875" y="1.253996875" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-0.999996875" y="0.999996875" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="MAXCAP_LX_0.1F_1PRIMARY">
<description>Original name &lt;b&gt;MAXCAP_LX_0.1F&lt;/b&gt;&lt;p&gt;</description>
<pad name="1" x="-2.54" y="0" drill="1.1938" diameter="1.6002"/>
<pad name="2" x="2.54" y="0" drill="1.1938" diameter="1.6002"/>
<circle x="0" y="0" radius="5.3594" width="0.2" layer="21"/>
<text x="-5.450003125" y="5.704003125" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-5.450003125" y="5.450003125" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="64-PM_1PRIMARY">
<description>Original name &lt;b&gt;64-PM&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="0" y="0" dx="8.7122" dy="8.7122" layer="1"/>
<smd name="2" x="-5.5372" y="3.7592" dx="0.254" dy="1.5494" layer="1" rot="R90"/>
<smd name="3" x="-5.5372" y="3.2512" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="4" x="-5.5372" y="2.7432" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="-5.5372" y="2.2606" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="-5.5372" y="1.7526" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="-5.5372" y="1.2446" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="-5.5372" y="0.762" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="-5.5372" y="0.254" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="-5.5372" y="-0.254" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="-5.5372" y="-0.762" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="-5.5372" y="-1.2446" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="-5.5372" y="-1.7526" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="-5.5372" y="-2.2606" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="-5.5372" y="-2.7432" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="-5.5372" y="-3.2512" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="-5.5372" y="-3.7592" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="18" x="-3.7592" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="19" x="-3.2512" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="20" x="-2.7432" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="21" x="-2.2606" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="22" x="-1.7526" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="23" x="-1.2446" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="24" x="-0.762" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="25" x="-0.254" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="26" x="0.254" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="27" x="0.762" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="28" x="1.2446" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="29" x="1.7526" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="30" x="2.2606" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="31" x="2.7432" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="32" x="3.2512" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="33" x="3.7592" y="-5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="34" x="5.5372" y="-3.7592" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="35" x="5.5372" y="-3.2512" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="36" x="5.5372" y="-2.7432" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="37" x="5.5372" y="-2.2606" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="38" x="5.5372" y="-1.7526" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="39" x="5.5372" y="-1.2446" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="40" x="5.5372" y="-0.762" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="41" x="5.5372" y="-0.254" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="42" x="5.5372" y="0.254" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="43" x="5.5372" y="0.762" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="44" x="5.5372" y="1.2446" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="45" x="5.5372" y="1.7526" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="46" x="5.5372" y="2.2606" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="47" x="5.5372" y="2.7432" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="48" x="5.5372" y="3.2512" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="49" x="5.5372" y="3.7592" dx="0.254" dy="1.5494" layer="1" roundness="100" rot="R90"/>
<smd name="50" x="3.7592" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="51" x="3.2512" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="52" x="2.7432" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="53" x="2.2606" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="54" x="1.7526" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="55" x="1.2446" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="56" x="0.762" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="57" x="0.254" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="58" x="-0.254" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="59" x="-0.762" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="60" x="-1.2446" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="61" x="-1.7526" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="62" x="-2.2606" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="63" x="-2.7432" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="64" x="-3.2512" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<smd name="65" x="-3.7592" y="5.5372" dx="0.254" dy="1.5494" layer="1" roundness="100"/>
<circle x="-6.2992" y="4.4958" radius="0.127" width="0.2499875" layer="21"/>
<wire x1="5.6896" y1="-5.6134" x2="5.6896" y2="-4.0132" width="0.2" layer="21"/>
<wire x1="-5.6896" y1="4.191" x2="-4.1148" y2="5.6896" width="0.2" layer="21"/>
<wire x1="4.064" y1="-5.6896" x2="5.6896" y2="-5.6896" width="0.2" layer="21"/>
<wire x1="-5.6896" y1="-5.6388" x2="-4.064" y2="-5.6388" width="0.2" layer="21"/>
<wire x1="-5.6642" y1="-5.6388" x2="-5.6642" y2="-4.064" width="0.2" layer="21"/>
<wire x1="5.6896" y1="4.0386" x2="5.6896" y2="5.588" width="0.2" layer="21"/>
<wire x1="4.0132" y1="5.6388" x2="5.6134" y2="5.6388" width="0.2" layer="21"/>
<text x="-6.54720625" y="6.6555875" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-7.81720625" y="-8.8384125" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SC70_1PRIMARY">
<description>Original name &lt;b&gt;SC70&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="-0.6604" y="-0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="2" x="0" y="-0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="3" x="0.6604" y="-0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="4" x="0.6604" y="0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="5" x="-0.6604" y="0.9398" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<circle x="-1.143" y="-1.1938" radius="0.1016" width="0.1" layer="21"/>
<wire x1="-0.889" y1="-0.4064" x2="-0.889" y2="0.4064" width="0.2" layer="21"/>
<wire x1="-0.889" y1="-0.4064" x2="0.889" y2="-0.4064" width="0.2" layer="21"/>
<wire x1="-0.889" y1="0.4064" x2="0.889" y2="0.4064" width="0.2" layer="21"/>
<wire x1="0.889" y1="-0.4064" x2="0.889" y2="0.4064" width="0.2" layer="21"/>
<text x="-1.299996875" y="1.76560625" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.299996875" y="1.51160625" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="16-PIN_QFN_FOR_ADXL362_1PRIMARY">
<description>Original name &lt;b&gt;16-pin_QFN_for_ADXL362&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="0" y="0" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="2" x="0" y="-0.508" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="3" x="0" y="-0.9906" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="4" x="0" y="-1.4986" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="5" x="0" y="-2.0066" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="6" x="0.7874" y="-2.3368" dx="0.3048" dy="0.7874" layer="1"/>
<smd name="7" x="1.2954" y="-2.3368" dx="0.3048" dy="0.7874" layer="1"/>
<smd name="8" x="1.8034" y="-2.3368" dx="0.3048" dy="0.7874" layer="1"/>
<smd name="9" x="2.5908" y="-2.0066" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="10" x="2.5908" y="-1.4986" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="11" x="2.5908" y="-0.9906" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="12" x="2.5908" y="-0.508" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="13" x="2.5908" y="0" dx="0.3048" dy="0.9144" layer="1" rot="R90"/>
<smd name="14" x="1.8034" y="0.3302" dx="0.3048" dy="0.7874" layer="1"/>
<smd name="15" x="1.2954" y="0.3302" dx="0.3048" dy="0.7874" layer="1"/>
<smd name="16" x="0.7874" y="0.3302" dx="0.3048" dy="0.7874" layer="1"/>
<wire x1="-0.4826" y1="-2.7178" x2="-0.2032" y2="-2.7178" width="0.2" layer="21"/>
<wire x1="-0.4826" y1="-2.7178" x2="-0.4826" y2="-2.4892" width="0.2" layer="21"/>
<wire x1="3.0734" y1="0.508" x2="3.0734" y2="0.762" width="0.2" layer="21"/>
<wire x1="2.794" y1="0.762" x2="3.0734" y2="0.762" width="0.2" layer="21"/>
<wire x1="2.8194" y1="-2.7178" x2="3.0734" y2="-2.7178" width="0.2" layer="21"/>
<wire x1="-0.4826" y1="0.4826" x2="-0.4826" y2="0.762" width="0.2" layer="21"/>
<wire x1="3.0734" y1="-2.7178" x2="3.0734" y2="-2.4384" width="0.2" layer="21"/>
<wire x1="-0.4826" y1="0.762" x2="-0.2286" y2="0.762" width="0.2" layer="21"/>
<text x="-1.124990625" y="1.1040125" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.124990625" y="0.8500125" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="0603_-_LED_1PRIMARY">
<description>Original name &lt;b&gt;0603_-_LED&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="0.8382" y="0" dx="0.9906" dy="1.0922" layer="1" rot="R90"/>
<smd name="2" x="-0.8382" y="0" dx="0.9906" dy="1.0922" layer="1" rot="R90"/>
<wire x1="-1.7018" y1="-0.4064" x2="-1.7018" y2="0.4064" width="0.2" layer="21"/>
<text x="-1.799996875" y="0.8556" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.799996875" y="0.6016" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SOT343R_1PRIMARY">
<description>Original name &lt;b&gt;SOT343R&lt;/b&gt;&lt;p&gt;</description>
<smd name="1" x="0.5842" y="-1.016" dx="0.6096" dy="0.9144" layer="1" rot="R180"/>
<smd name="2" x="-0.6604" y="-1.016" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="3" x="-0.6604" y="1.0922" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<smd name="4" x="0.635" y="1.0922" dx="0.4064" dy="0.9144" layer="1" rot="R180"/>
<wire x1="0.889" y1="-0.4572" x2="0.889" y2="0.5334" width="0.2" layer="21"/>
<wire x1="-0.9144" y1="-0.4572" x2="0.889" y2="-0.4572" width="0.2" layer="21"/>
<wire x1="-0.9144" y1="-0.4572" x2="-0.9144" y2="0.5334" width="0.2" layer="21"/>
<wire x1="-0.9144" y1="0.5334" x2="0.889" y2="0.5334" width="0.2" layer="21"/>
<text x="-1.01409375" y="1.908403125" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.01409375" y="1.654403125" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
<package name="SMA_PORT_1PRIMARY">
<description>Original name &lt;b&gt;SMA_Port&lt;/b&gt;&lt;p&gt;</description>
<pad name="1" x="0" y="1.8034" drill="1.016" diameter="1.397"/>
<pad name="2" x="0" y="-1.8034" drill="1.016" diameter="1.397"/>
<wire x1="-1.016" y1="0" x2="1.016" y2="0" width="0.127" layer="21"/>
<text x="-1.0795" y="2.850109375" size="2.032" layer="25" font="vector" ratio="13" rot="SR0">&gt;NAME</text>
<text x="-1.0795" y="2.596109375" size="2.032" layer="27" font="vector" ratio="13" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NLSV1T244">
<wire x1="-7.62" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="VCCA" x="-12.7" y="2.54" length="middle"/>
<pin name="A" x="-12.7" y="-2.54" length="middle" function="clk"/>
<pin name="GND" x="-12.7" y="-7.62" length="middle"/>
<pin name="OE" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="B" x="15.24" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="VCCB" x="15.24" y="2.54" length="middle" rot="R180"/>
<text x="-7.112" y="8.636" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="NCP583">
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="CE" x="-12.7" y="2.54" length="middle"/>
<pin name="GND" x="-12.7" y="-2.54" length="middle"/>
<pin name="VOUT" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="VIN" x="12.7" y="2.54" length="middle" rot="R180"/>
<text x="-7.366" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="NCP300LSN20T1G">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<pin name="OUT" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="IN" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="GND" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="NC@1" x="-12.7" y="7.62" length="middle"/>
<pin name="NC@2" x="-12.7" y="-7.62" length="middle"/>
<text x="-7.366" y="11.684" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="SST25WF040">
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="CE#" x="-15.24" y="7.62" length="middle"/>
<pin name="SO" x="-15.24" y="2.54" length="middle"/>
<pin name="WP#" x="-15.24" y="-2.54" length="middle"/>
<pin name="VSS" x="-15.24" y="-7.62" length="middle"/>
<pin name="SI" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="SCK" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="RST/HOLD" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="7.62" length="middle" rot="R180"/>
<text x="-9.906" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="NTK3134N">
<wire x1="-0.762" y1="-1.778" x2="0" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.778" x2="-2.54" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.778" x2="-1.905" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-1.778" x2="2.54" y2="-1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.778" x2="3.175" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="1.905" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-6.35" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="3" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short"/>
<polygon width="0.1524" layer="94">
<vertex x="0" y="-1.524"/>
<vertex x="-0.762" y="-0.508"/>
<vertex x="0.762" y="-0.508"/>
</polygon>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="ESD5Z3.3T1">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="2.032" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.905" y2="-2.032" width="0.254" layer="94"/>
<text x="-2.286" y="2.413" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="FC-135">
<wire x1="0" y1="1.016" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-0.381" x2="1.524" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-0.381" x2="1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="1.524" y1="0.381" x2="-1.524" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.381" x2="-1.524" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-1.016" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.016" x2="1.778" y2="-1.016" width="0.1524" layer="94"/>
<text x="-1.016" y="3.048" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="96">&gt;Value</text>
<pin name="1" x="5.08" y="-2.54" length="middle" rot="R180"/>
<pin name="2" x="5.08" y="2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="SCHOTTKEY_3">
<text x="2.159" y="2.413" size="1.778" layer="95">&gt;NAME</text>
<text x="2.159" y="-0.381" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="1.27" y1="-3.81" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.016" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.016" y1="-1.905" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<pin name="1" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="1.27" y1="1.27" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.016" y2="4.445" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.016" y1="3.175" x2="1.27" y2="3.175" width="0.254" layer="94"/>
<pin name="2" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="3" x="-5.08" y="0" visible="off" length="middle" direction="pas"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="94"/>
</symbol>
<symbol name="L">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="VARCAP">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="2.54" width="0.127" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.286" y2="2.032" width="0.127" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.032" y2="2.54" width="0.127" layer="94"/>
</symbol>
<symbol name="NPNFLIP">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-10.16" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="PROBE">
<circle x="0" y="0" radius="1.27" width="1.016" layer="94"/>
<text x="-4.572" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="0" visible="off" length="short"/>
</symbol>
<symbol name="M10">
<wire x1="6.35" y1="-20.32" x2="0" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-12.7" x2="5.08" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="5.08" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-17.78" x2="5.08" y2="-17.78" width="0.6096" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-20.32" x2="6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="0" y1="7.62" x2="6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="5.08" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="5.08" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<text x="0" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="10.16" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="10.16" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="10.16" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="M14">
<wire x1="6.35" y1="-20.32" x2="0" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-12.7" x2="5.08" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="5.08" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-17.78" x2="5.08" y2="-17.78" width="0.6096" layer="94"/>
<wire x1="0" y1="17.78" x2="0" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-20.32" x2="6.35" y2="17.78" width="0.4064" layer="94"/>
<wire x1="0" y1="17.78" x2="6.35" y2="17.78" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="5.08" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="5.08" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94"/>
<wire x1="3.81" y1="12.7" x2="5.08" y2="12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="15.24" x2="5.08" y2="15.24" width="0.6096" layer="94"/>
<text x="0" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="18.542" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="10.16" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="10.16" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="10.16" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="10.16" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="10.16" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="10.16" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="14" x="10.16" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M04">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="MSP430F261X">
<wire x1="-30.48" y1="-30.48" x2="-30.48" y2="27.94" width="0.254" layer="94"/>
<wire x1="-30.48" y1="27.94" x2="27.94" y2="27.94" width="0.254" layer="94"/>
<wire x1="27.94" y1="27.94" x2="27.94" y2="-30.48" width="0.254" layer="94"/>
<wire x1="27.94" y1="-30.48" x2="-30.48" y2="-30.48" width="0.254" layer="94"/>
<pin name="DVCCL" x="-35.56" y="17.78" length="middle"/>
<pin name="P6.3/A3" x="-35.56" y="15.24" length="middle"/>
<pin name="P6.4/A4" x="-35.56" y="12.7" length="middle"/>
<pin name="P6.5/A5/DAC1" x="-35.56" y="10.16" length="middle"/>
<pin name="P6.6/A6/DAC0" x="-35.56" y="7.62" length="middle"/>
<pin name="P6.7/A7/DAC1/SVSIN" x="-35.56" y="5.08" length="middle"/>
<pin name="VREF+" x="-35.56" y="2.54" length="middle"/>
<pin name="XIN" x="-35.56" y="0" length="middle"/>
<pin name="XOUT" x="-35.56" y="-2.54" length="middle"/>
<pin name="VEREF+/DAC0" x="-35.56" y="-5.08" length="middle"/>
<pin name="VREF-/VEREF-" x="-35.56" y="-7.62" length="middle"/>
<pin name="P1.0/TACLK/CAOUT" x="-35.56" y="-10.16" length="middle"/>
<pin name="P1.1/TA0" x="-35.56" y="-12.7" length="middle"/>
<pin name="P1.2/TA1" x="-35.56" y="-15.24" length="middle"/>
<pin name="P1.3/TA2" x="-35.56" y="-17.78" length="middle"/>
<pin name="P1.4/SMCLK" x="-35.56" y="-20.32" length="middle"/>
<pin name="P1.5/TA0" x="-20.32" y="-35.56" length="middle" rot="R90"/>
<pin name="P1.6/TA1" x="-17.78" y="-35.56" length="middle" rot="R90"/>
<pin name="P1.7/TA2" x="-15.24" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.0/ACLK/CA2" x="-12.7" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.1/TAINCLK/CA3" x="-10.16" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.2" x="-7.62" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.3" x="-5.08" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.4" x="-2.54" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.5" x="0" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.6" x="2.54" y="-35.56" length="middle" rot="R90"/>
<pin name="P2.7" x="5.08" y="-35.56" length="middle" rot="R90"/>
<pin name="P3.0" x="7.62" y="-35.56" length="middle" rot="R90"/>
<pin name="P3.1" x="10.16" y="-35.56" length="middle" rot="R90"/>
<pin name="P3.2" x="12.7" y="-35.56" length="middle" rot="R90"/>
<pin name="P3.3" x="15.24" y="-35.56" length="middle" rot="R90"/>
<pin name="P3.4" x="17.78" y="-35.56" length="middle" rot="R90"/>
<pin name="P3.5" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="P3.6" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="P3.7" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="P4.0" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="P4.1" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="P4.2" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="P4.3" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="P4.4" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="P4.5" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="P4.6" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="P4.7" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="P5.0" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="P5.1" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="P5.2" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="P5.3" x="33.02" y="15.24" length="middle" rot="R180"/>
<pin name="P5.4" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="P5.5" x="17.78" y="33.02" length="middle" rot="R270"/>
<pin name="P5.6" x="15.24" y="33.02" length="middle" rot="R270"/>
<pin name="P5.7" x="12.7" y="33.02" length="middle" rot="R270"/>
<pin name="XT2OUT" x="10.16" y="33.02" length="middle" rot="R270"/>
<pin name="XT2IN" x="7.62" y="33.02" length="middle" rot="R270"/>
<pin name="TDO/TDI" x="5.08" y="33.02" length="middle" rot="R270"/>
<pin name="TDI/TCLK" x="2.54" y="33.02" length="middle" rot="R270"/>
<pin name="TMS" x="0" y="33.02" length="middle" rot="R270"/>
<pin name="TCK" x="-2.54" y="33.02" length="middle" rot="R270"/>
<pin name="!RST" x="-5.08" y="33.02" length="middle" rot="R270"/>
<pin name="P6.0/A0" x="-7.62" y="33.02" length="middle" rot="R270"/>
<pin name="P6.1/A1" x="-10.16" y="33.02" length="middle" rot="R270"/>
<pin name="P6.2/A2" x="-12.7" y="33.02" length="middle" rot="R270"/>
<pin name="AVSS" x="-15.24" y="33.02" length="middle" rot="R270"/>
<pin name="DVSS1" x="-17.78" y="33.02" length="middle" rot="R270"/>
<pin name="AVCC" x="-20.32" y="33.02" length="middle" rot="R270"/>
<pin name="VSS" x="0" y="0" length="middle"/>
<text x="27.94" y="30.48" size="1.778" layer="95">&gt;NAME</text>
<text x="27.94" y="27.94" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="TS5A3166">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="NO" x="-12.7" y="5.08" length="middle"/>
<pin name="COM" x="-12.7" y="0" length="middle"/>
<pin name="GND" x="-12.7" y="-5.08" length="middle"/>
<pin name="VCC" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="IN" x="12.7" y="-5.08" length="middle" rot="R180"/>
<text x="-7.366" y="8.636" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LM94021">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="GS2" x="-12.7" y="5.08" length="middle"/>
<pin name="GND" x="-12.7" y="0" length="middle"/>
<pin name="OUT" x="-12.7" y="-5.08" length="middle"/>
<text x="-7.366" y="8.636" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="GS1" x="12.7" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="ADXL362">
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="-12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<pin name="VDDI/O" x="-17.78" y="7.62" length="middle"/>
<pin name="NC@1" x="-17.78" y="2.54" length="middle"/>
<pin name="RSVRD@1" x="-17.78" y="-2.54" length="middle"/>
<pin name="SCLK" x="-17.78" y="-7.62" length="middle"/>
<pin name="RSVRD@2" x="-17.78" y="-12.7" length="middle"/>
<pin name="MOSI" x="-5.08" y="-20.32" length="middle" rot="R90"/>
<pin name="MISO" x="0" y="-20.32" length="middle" rot="R90"/>
<pin name="CS" x="5.08" y="-20.32" length="middle" rot="R90"/>
<pin name="INT2" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="RSVRD@3" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="INT1" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="GND@1" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="GND@2" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="VS" x="5.08" y="15.24" length="middle" rot="R270"/>
<pin name="NC@2" x="0" y="15.24" length="middle" rot="R270"/>
<pin name="GND@3" x="-5.08" y="15.24" length="middle" rot="R270"/>
<text x="10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="BTRANS">
<wire x1="-0.762" y1="0.762" x2="0" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="0.762" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0.762" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-1.905" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.762" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.762" x2="3.175" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="4.445" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-2.54" visible="pad" length="short" rot="R90"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="2" x="-5.08" y="2.54" visible="pad" length="short"/>
<polygon width="0.1524" layer="94">
<vertex x="0" y="1.016"/>
<vertex x="-0.762" y="2.032"/>
<vertex x="0.762" y="2.032"/>
</polygon>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<pin name="4" x="0" y="2.54" visible="off" length="point"/>
</symbol>
<symbol name="NCS2200SQ2T2G">
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.8862" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="3.9116" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<text x="2.54" y="6.985" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<text x="2.54" y="5.08" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="1" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="5" x="-2.54" y="7.62" visible="pad" length="short" direction="in" rot="R270"/>
<pin name="2" x="-2.54" y="-7.62" visible="pad" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="SMAPORT">
<wire x1="-2.54" y1="-7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<pin name="P1" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="P2" x="10.16" y="-5.08" length="middle" rot="R180"/>
<text x="-4.572" y="8.636" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NLSV1T244">
<gates>
<gate name="G$1" symbol="NLSV1T244" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UDFN6_1PRIMARY">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="B" pad="5"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="OE" pad="4"/>
<connect gate="G$1" pin="VCCA" pad="1"/>
<connect gate="G$1" pin="VCCB" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NCP583" prefix="U">
<gates>
<gate name="G$1" symbol="NCP583" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC82-AB_1PRIMARY">
<connects>
<connect gate="G$1" pin="CE" pad="1"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="4"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NCP300LSN20T1G">
<gates>
<gate name="G$1" symbol="NCP300LSN20T1G" x="0" y="0"/>
</gates>
<devices>
<device name="&quot;" package="SOT-23-5_1PRIMARY">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="NC@1" pad="4"/>
<connect gate="G$1" pin="NC@2" pad="5"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SST25WF040">
<gates>
<gate name="G$1" symbol="SST25WF040" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC8_1PRIMARY">
<connects>
<connect gate="G$1" pin="CE#" pad="1"/>
<connect gate="G$1" pin="RST/HOLD" pad="7"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="SI" pad="5"/>
<connect gate="G$1" pin="SO" pad="2"/>
<connect gate="G$1" pin="VDD" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP#" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NTK3134N">
<gates>
<gate name="G$1" symbol="NTK3134N" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT723_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESD5Z3.3T1">
<gates>
<gate name="G$1" symbol="ESD5Z3.3T1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-523_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0603_-_RES_1" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603_-_RES_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0805_1" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="0805_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FC-135">
<gates>
<gate name="G$1" symbol="FC-135" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FC-135_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0402_1" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="0402_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RB751S40T1">
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-523_1PRIMARY">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HSMS-285C">
<gates>
<gate name="G$1" symbol="SCHOTTKEY_3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-323,_HSMS-285C_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0603_1" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VAR-CAP_603_1" uservalue="yes">
<gates>
<gate name="G$1" symbol="VARCAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VAR-CAP_603_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BFT25A">
<gates>
<gate name="G$1" symbol="NPNFLIP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23_1PRIMARY">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0402RES" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0402_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PROBE_POINT_1">
<gates>
<gate name="G$1" symbol="PROBE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PROBE_POINT_2PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PROBE_POINT_2">
<gates>
<gate name="G$1" symbol="PROBE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PROBE_POINT_2PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MHDR1X10_1">
<gates>
<gate name="G$1" symbol="M10" x="-2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="MHDR1X10_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MHDR1X14_1">
<gates>
<gate name="G$1" symbol="M14" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="MHDR1X14_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MHDR1X4_1">
<gates>
<gate name="G$1" symbol="M04" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="MHDR1X4_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAXCAP_LX_0.1F_1" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MAXCAP_LX_0.1F_1PRIMARY">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MSP430F261X">
<gates>
<gate name="G$1" symbol="MSP430F261X" x="0" y="0"/>
</gates>
<devices>
<device name="" package="64-PM_1PRIMARY">
<connects>
<connect gate="G$1" pin="!RST" pad="59"/>
<connect gate="G$1" pin="AVCC" pad="65"/>
<connect gate="G$1" pin="AVSS" pad="63"/>
<connect gate="G$1" pin="DVCCL" pad="2"/>
<connect gate="G$1" pin="DVSS1" pad="64"/>
<connect gate="G$1" pin="P1.0/TACLK/CAOUT" pad="13"/>
<connect gate="G$1" pin="P1.1/TA0" pad="14"/>
<connect gate="G$1" pin="P1.2/TA1" pad="15"/>
<connect gate="G$1" pin="P1.3/TA2" pad="16"/>
<connect gate="G$1" pin="P1.4/SMCLK" pad="17"/>
<connect gate="G$1" pin="P1.5/TA0" pad="18"/>
<connect gate="G$1" pin="P1.6/TA1" pad="19"/>
<connect gate="G$1" pin="P1.7/TA2" pad="20"/>
<connect gate="G$1" pin="P2.0/ACLK/CA2" pad="21"/>
<connect gate="G$1" pin="P2.1/TAINCLK/CA3" pad="22"/>
<connect gate="G$1" pin="P2.2" pad="23"/>
<connect gate="G$1" pin="P2.3" pad="24"/>
<connect gate="G$1" pin="P2.4" pad="25"/>
<connect gate="G$1" pin="P2.5" pad="26"/>
<connect gate="G$1" pin="P2.6" pad="27"/>
<connect gate="G$1" pin="P2.7" pad="28"/>
<connect gate="G$1" pin="P3.0" pad="29"/>
<connect gate="G$1" pin="P3.1" pad="30"/>
<connect gate="G$1" pin="P3.2" pad="31"/>
<connect gate="G$1" pin="P3.3" pad="32"/>
<connect gate="G$1" pin="P3.4" pad="33"/>
<connect gate="G$1" pin="P3.5" pad="34"/>
<connect gate="G$1" pin="P3.6" pad="35"/>
<connect gate="G$1" pin="P3.7" pad="36"/>
<connect gate="G$1" pin="P4.0" pad="37"/>
<connect gate="G$1" pin="P4.1" pad="38"/>
<connect gate="G$1" pin="P4.2" pad="39"/>
<connect gate="G$1" pin="P4.3" pad="40"/>
<connect gate="G$1" pin="P4.4" pad="41"/>
<connect gate="G$1" pin="P4.5" pad="42"/>
<connect gate="G$1" pin="P4.6" pad="43"/>
<connect gate="G$1" pin="P4.7" pad="44"/>
<connect gate="G$1" pin="P5.0" pad="45"/>
<connect gate="G$1" pin="P5.1" pad="46"/>
<connect gate="G$1" pin="P5.2" pad="47"/>
<connect gate="G$1" pin="P5.3" pad="48"/>
<connect gate="G$1" pin="P5.4" pad="49"/>
<connect gate="G$1" pin="P5.5" pad="50"/>
<connect gate="G$1" pin="P5.6" pad="51"/>
<connect gate="G$1" pin="P5.7" pad="52"/>
<connect gate="G$1" pin="P6.0/A0" pad="60"/>
<connect gate="G$1" pin="P6.1/A1" pad="61"/>
<connect gate="G$1" pin="P6.2/A2" pad="62"/>
<connect gate="G$1" pin="P6.3/A3" pad="3"/>
<connect gate="G$1" pin="P6.4/A4" pad="4"/>
<connect gate="G$1" pin="P6.5/A5/DAC1" pad="5"/>
<connect gate="G$1" pin="P6.6/A6/DAC0" pad="6"/>
<connect gate="G$1" pin="P6.7/A7/DAC1/SVSIN" pad="7"/>
<connect gate="G$1" pin="TCK" pad="58"/>
<connect gate="G$1" pin="TDI/TCLK" pad="56"/>
<connect gate="G$1" pin="TDO/TDI" pad="55"/>
<connect gate="G$1" pin="TMS" pad="57"/>
<connect gate="G$1" pin="VEREF+/DAC0" pad="11"/>
<connect gate="G$1" pin="VREF+" pad="8"/>
<connect gate="G$1" pin="VREF-/VEREF-" pad="12"/>
<connect gate="G$1" pin="VSS" pad="1"/>
<connect gate="G$1" pin="XIN" pad="9"/>
<connect gate="G$1" pin="XOUT" pad="10"/>
<connect gate="G$1" pin="XT2IN" pad="54"/>
<connect gate="G$1" pin="XT2OUT" pad="53"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TS5A3166">
<gates>
<gate name="G$1" symbol="TS5A3166" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC70-5_1PRIMARY">
<connects>
<connect gate="G$1" pin="COM" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="IN" pad="5"/>
<connect gate="G$1" pin="NO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM94021">
<gates>
<gate name="G$1" symbol="LM94021" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC70_1PRIMARY">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="GS1" pad="5"/>
<connect gate="G$1" pin="GS2" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADXL362">
<gates>
<gate name="G$1" symbol="ADXL362" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="16-PIN_QFN_FOR_ADXL362_1PRIMARY">
<connects>
<connect gate="G$1" pin="CS" pad="8"/>
<connect gate="G$1" pin="GND@1" pad="12"/>
<connect gate="G$1" pin="GND@2" pad="13"/>
<connect gate="G$1" pin="GND@3" pad="16"/>
<connect gate="G$1" pin="INT1" pad="11"/>
<connect gate="G$1" pin="INT2" pad="9"/>
<connect gate="G$1" pin="MISO" pad="7"/>
<connect gate="G$1" pin="MOSI" pad="6"/>
<connect gate="G$1" pin="NC@1" pad="2"/>
<connect gate="G$1" pin="NC@2" pad="15"/>
<connect gate="G$1" pin="RSVRD@1" pad="3"/>
<connect gate="G$1" pin="RSVRD@2" pad="5"/>
<connect gate="G$1" pin="RSVRD@3" pad="10"/>
<connect gate="G$1" pin="SCLK" pad="4"/>
<connect gate="G$1" pin="VDDI/O" pad="1"/>
<connect gate="G$1" pin="VS" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP0603" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603_1PRIMARY">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SML-311UTT86">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603_-_LED_1PRIMARY">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BF1212WR">
<gates>
<gate name="G$1" symbol="BTRANS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT343R_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="3"/>
<connect gate="G$1" pin="2" pad="1"/>
<connect gate="G$1" pin="3" pad="2"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NCS2200SQ2T2G">
<gates>
<gate name="G$1" symbol="NCS2200SQ2T2G" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="SC70-5_1PRIMARY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMA_PORT_1">
<gates>
<gate name="G$1" symbol="SMAPORT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMA_PORT_1PRIMARY">
<connects>
<connect gate="G$1" pin="P1" pad="1"/>
<connect gate="G$1" pin="P2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="AUTHOR" value="Josiah Hester"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U2" library="Moo1.2" deviceset="NLSV1T244" device=""/>
<part name="Q2" library="Moo1.2" deviceset="NTK3134N" device=""/>
<part name="ED" library="Moo1.2" deviceset="ESD5Z3.3T1" device=""/>
<part name="RS" library="Moo1.2" deviceset="0603_-_RES_1" device="" value="10"/>
<part name="C1" library="Moo1.2" deviceset="0805_1" device="" value="10uF"/>
<part name="U3" library="Moo1.2" deviceset="NCP583" device=""/>
<part name="U5" library="Moo1.2" deviceset="NCP300LSN20T1G" device="&quot;"/>
<part name="U4" library="Moo1.2" deviceset="NLSV1T244" device=""/>
<part name="U7" library="Moo1.2" deviceset="SST25WF040" device=""/>
<part name="Y1" library="Moo1.2" deviceset="FC-135" device=""/>
<part name="C2" library="Moo1.2" deviceset="0402_1" device="" value="0.1uF"/>
<part name="D6" library="Moo1.2" deviceset="RB751S40T1" device=""/>
<part name="D7" library="Moo1.2" deviceset="RB751S40T1" device=""/>
<part name="GND1" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND13" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND14" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="D5" library="Moo1.2" deviceset="HSMS-285C" device=""/>
<part name="D4" library="Moo1.2" deviceset="HSMS-285C" device=""/>
<part name="D3" library="Moo1.2" deviceset="HSMS-285C" device=""/>
<part name="D2" library="Moo1.2" deviceset="HSMS-285C" device=""/>
<part name="D1" library="Moo1.2" deviceset="HSMS-285C" device=""/>
<part name="CP1" library="Moo1.2" deviceset="0402_1" device="" value="10pF"/>
<part name="CP2" library="Moo1.2" deviceset="0402_1" device="" value="10pF"/>
<part name="CP3" library="Moo1.2" deviceset="0402_1" device="" value="10pF"/>
<part name="CP4" library="Moo1.2" deviceset="0402_1" device="" value="10pF"/>
<part name="CS1" library="Moo1.2" deviceset="0402_1" device="" value="7pF"/>
<part name="CS2" library="Moo1.2" deviceset="0402_1" device="" value="7pF"/>
<part name="CS3" library="Moo1.2" deviceset="0402_1" device="" value="7pF"/>
<part name="CS4" library="Moo1.2" deviceset="0402_1" device="" value="7pF"/>
<part name="CS5" library="Moo1.2" deviceset="0402_1" device="" value="7pF"/>
<part name="L1" library="Moo1.2" deviceset="0603_1" device="" value="6.8nH"/>
<part name="CV1" library="Moo1.2" deviceset="VAR-CAP_603_1" device="" value="7-30pF Do not stuff"/>
<part name="Q1" library="Moo1.2" deviceset="BFT25A" device=""/>
<part name="R3" library="Moo1.2" deviceset="0402RES" device="" value="2k"/>
<part name="PP2" library="Moo1.2" deviceset="PROBE_POINT_1" device=""/>
<part name="PP1" library="Moo1.2" deviceset="PROBE_POINT_2" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device="" value="antennaefrontend"/>
<part name="FRAME2" library="frames" deviceset="A4L-LOC" device="" value="digital"/>
<part name="FRAME3" library="frames" deviceset="A4L-LOC" device="" value="sensors"/>
<part name="C20" library="Moo1.2" deviceset="0402_1" device="" value="1uF"/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R10" library="Moo1.2" deviceset="0402RES" device="" value="10k"/>
<part name="R11" library="Moo1.2" deviceset="0402RES" device="" value="10k"/>
<part name="P1" library="Moo1.2" deviceset="MHDR1X10_1" device=""/>
<part name="GND8" library="Moo1.2" deviceset="GND" device=""/>
<part name="P2" library="Moo1.2" deviceset="MHDR1X14_1" device=""/>
<part name="P3" library="Moo1.2" deviceset="MHDR1X14_1" device=""/>
<part name="P4" library="Moo1.2" deviceset="MHDR1X4_1" device=""/>
<part name="GND9" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND18" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND19" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND20" library="Moo1.2" deviceset="GND" device=""/>
<part name="C14" library="Moo1.2" deviceset="0402_1" device="" value="1uF"/>
<part name="R5" library="Moo1.2" deviceset="0402RES" device="" value="100"/>
<part name="R4" library="Moo1.2" deviceset="0402RES" device="" value="1k"/>
<part name="C7" library="Moo1.2" deviceset="MAXCAP_LX_0.1F_1" device="" value="MaxCap LX 0.1F"/>
<part name="GND21" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND22" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND23" library="Moo1.2" deviceset="GND" device=""/>
<part name="MCU1" library="Moo1.2" deviceset="MSP430F261X" device=""/>
<part name="C6" library="Moo1.2" deviceset="0402_1" device="" value="10uF"/>
<part name="C5" library="Moo1.2" deviceset="0402_1" device="" value="0.1uF"/>
<part name="GND24" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND25" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND26" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND27" library="Moo1.2" deviceset="GND" device=""/>
<part name="R1" library="Moo1.2" deviceset="0402RES" device="" value="0"/>
<part name="GND28" library="Moo1.2" deviceset="GND" device=""/>
<part name="R6" library="Moo1.2" deviceset="0402RES" device="" value="33k"/>
<part name="GND29" library="Moo1.2" deviceset="GND" device=""/>
<part name="R16" library="Moo1.2" deviceset="0402RES" device="" value="200k"/>
<part name="R18" library="Moo1.2" deviceset="0402RES" device="" value="1M"/>
<part name="C11" library="Moo1.2" deviceset="0402_1" device="" value="10pF"/>
<part name="GND30" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND31" library="Moo1.2" deviceset="GND" device=""/>
<part name="U6" library="Moo1.2" deviceset="TS5A3166" device=""/>
<part name="TMP" library="Moo1.2" deviceset="LM94021" device=""/>
<part name="XL1" library="Moo1.2" deviceset="ADXL362" device=""/>
<part name="Q5" library="Moo1.2" deviceset="NTK3134N" device=""/>
<part name="R19" library="Moo1.2" deviceset="0402RES" device="" value="66.5k"/>
<part name="R17" library="Moo1.2" deviceset="0402RES" device="" value="33k"/>
<part name="C10" library="Moo1.2" deviceset="0402_1" device="" value="10pF"/>
<part name="GND32" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND33" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND34" library="Moo1.2" deviceset="GND" device=""/>
<part name="C13" library="Moo1.2" deviceset="0402_1" device="" value="0.1uF"/>
<part name="C12" library="Moo1.2" deviceset="0402_1" device="" value="100pF"/>
<part name="GND35" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND36" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND37" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND38" library="Moo1.2" deviceset="GND" device=""/>
<part name="C4" library="Moo1.2" deviceset="0402_1" device="" value="0.1uF"/>
<part name="GND39" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND40" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND41" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND42" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND43" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND44" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND45" library="Moo1.2" deviceset="GND" device=""/>
<part name="C3" library="Moo1.2" deviceset="CAP0603" device="" value="1uF"/>
<part name="Q3" library="Moo1.2" deviceset="NTK3134N" device=""/>
<part name="R2" library="Moo1.2" deviceset="0402RES" device="" value="1k"/>
<part name="GND46" library="Moo1.2" deviceset="GND" device=""/>
<part name="DA1" library="Moo1.2" deviceset="SML-311UTT86" device=""/>
<part name="Q4" library="Moo1.2" deviceset="BF1212WR" device=""/>
<part name="U1" library="Moo1.2" deviceset="NCS2200SQ2T2G" device=""/>
<part name="GND6" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND15" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND16" library="Moo1.2" deviceset="GND" device=""/>
<part name="GND17" library="Moo1.2" deviceset="GND" device=""/>
<part name="SMA1" library="Moo1.2" deviceset="SMA_PORT_1" device=""/>
<part name="GND47" library="Moo1.2" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="86.36" y="-27.94" size="1.778" layer="97">Designed by: Dan Yeager &amp; Alanson Sample
Copyright 2009 Intel Corporation</text>
<text x="139.7" y="-20.32" size="2.1844" layer="94">Moo 1.2 Antenna Front End</text>
<text x="86.36" y="-38.1" size="1.016" layer="94">Converted to EAGLE by: Josiah Hester
Contact jhester@clemson.edu if errors are found.
</text>
</plain>
<instances>
<instance part="U2" gate="G$1" x="129.54" y="106.68"/>
<instance part="Q2" gate="G$1" x="68.58" y="93.98"/>
<instance part="ED" gate="G$1" x="147.32" y="66.04" rot="R90"/>
<instance part="RS" gate="G$1" x="121.92" y="73.66" rot="R180"/>
<instance part="C1" gate="G$1" x="132.08" y="68.58" rot="R180"/>
<instance part="C2" gate="G$1" x="53.34" y="111.76"/>
<instance part="D6" gate="G$1" x="78.74" y="73.66"/>
<instance part="D7" gate="G$1" x="33.02" y="109.22" rot="R90"/>
<instance part="GND1" gate="1" x="147.32" y="58.42"/>
<instance part="GND2" gate="1" x="132.08" y="58.42"/>
<instance part="GND3" gate="1" x="147.32" y="91.44"/>
<instance part="GND4" gate="1" x="114.3" y="91.44"/>
<instance part="GND5" gate="1" x="60.96" y="88.9"/>
<instance part="GND10" gate="1" x="-30.48" y="-22.86"/>
<instance part="GND11" gate="1" x="-50.8" y="-25.4"/>
<instance part="GND12" gate="1" x="53.34" y="2.54"/>
<instance part="GND13" gate="1" x="-7.62" y="-7.62"/>
<instance part="GND14" gate="1" x="33.02" y="-2.54"/>
<instance part="D5" gate="G$1" x="33.02" y="66.04"/>
<instance part="D4" gate="G$1" x="33.02" y="50.8"/>
<instance part="D3" gate="G$1" x="33.02" y="35.56"/>
<instance part="D2" gate="G$1" x="33.02" y="20.32"/>
<instance part="D1" gate="G$1" x="33.02" y="5.08"/>
<instance part="CP1" gate="G$1" x="53.34" y="7.62"/>
<instance part="CP2" gate="G$1" x="53.34" y="22.86"/>
<instance part="CP3" gate="G$1" x="53.34" y="38.1"/>
<instance part="CP4" gate="G$1" x="53.34" y="53.34"/>
<instance part="CS1" gate="G$1" x="22.86" y="5.08" rot="R90"/>
<instance part="CS2" gate="G$1" x="22.86" y="20.32" rot="R90"/>
<instance part="CS3" gate="G$1" x="22.86" y="35.56" rot="R90"/>
<instance part="CS4" gate="G$1" x="22.86" y="50.8" rot="R90"/>
<instance part="CS5" gate="G$1" x="22.86" y="66.04" rot="R90"/>
<instance part="L1" gate="G$1" x="5.08" y="5.08" rot="R90"/>
<instance part="CV1" gate="G$1" x="-7.62" y="-2.54"/>
<instance part="Q1" gate="G$1" x="-33.02" y="-12.7"/>
<instance part="R3" gate="G$1" x="-15.24" y="-12.7" rot="MR0"/>
<instance part="PP2" gate="G$1" x="-15.24" y="-22.86"/>
<instance part="PP1" gate="G$1" x="25.4" y="-20.32" rot="R270"/>
<instance part="FRAME1" gate="G$1" x="-77.216" y="-41.148"/>
<instance part="Q4" gate="G$1" x="-48.26" y="-17.78" rot="R90"/>
<instance part="U1" gate="G$1" x="91.44" y="104.14"/>
<instance part="GND6" gate="1" x="53.34" y="106.68"/>
<instance part="GND15" gate="1" x="53.34" y="48.26"/>
<instance part="GND16" gate="1" x="53.34" y="33.02"/>
<instance part="GND17" gate="1" x="53.34" y="17.78"/>
<instance part="SMA1" gate="G$1" x="-60.96" y="38.1"/>
<instance part="GND47" gate="1" x="-45.72" y="30.48"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="147.32" y1="99.06" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="OE"/>
<wire x1="147.32" y1="99.06" x2="144.78" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="114.3" y1="99.06" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="116.84" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="60.96" y1="93.98" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="Q2" gate="G$1" pin="2"/>
<wire x1="63.5" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="ED" gate="G$1" pin="2"/>
<wire x1="147.32" y1="60.96" x2="147.32" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="1"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CV1" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="E"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="-30.48" y1="-20.32" x2="-30.48" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-22.86" x2="-22.86" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-20.32" x2="-30.48" y2="-20.32" width="0.1524" layer="91"/>
<junction x="-30.48" y="-20.32"/>
<pinref part="PP2" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="-22.86" x2="-22.86" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="132.08" y1="63.5" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CP4" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CP3" gate="G$1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CP2" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CP1" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="SMA1" gate="G$1" pin="P2"/>
<wire x1="-50.8" y1="33.02" x2="-45.72" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND47" gate="1" pin="GND"/>
</segment>
</net>
<net name="VREG" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VCCB"/>
<wire x1="144.78" y1="109.22" x2="147.32" y2="109.22" width="0.1524" layer="91"/>
<wire x1="147.32" y1="109.22" x2="147.32" y2="119.38" width="0.1524" layer="91"/>
<label x="144.78" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="BITPWR" class="0">
<segment>
<wire x1="114.3" y1="109.22" x2="114.3" y2="116.84" width="0.1524" layer="91"/>
<wire x1="114.3" y1="116.84" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
<wire x1="88.9" y1="116.84" x2="71.12" y2="116.84" width="0.1524" layer="91"/>
<wire x1="71.12" y1="106.68" x2="71.12" y2="116.84" width="0.1524" layer="91"/>
<label x="48.26" y="116.84" size="1.778" layer="95"/>
<wire x1="33.02" y1="116.84" x2="53.34" y2="116.84" width="0.1524" layer="91"/>
<junction x="71.12" y="116.84"/>
<pinref part="U2" gate="G$1" pin="VCCA"/>
<wire x1="53.34" y1="116.84" x2="71.12" y2="116.84" width="0.1524" layer="91"/>
<wire x1="116.84" y1="109.22" x2="114.3" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="5"/>
<wire x1="88.9" y1="111.76" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
<junction x="88.9" y="116.84"/>
<pinref part="U1" gate="G$1" pin="3"/>
<wire x1="83.82" y1="106.68" x2="71.12" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<junction x="53.34" y="116.84"/>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="33.02" y1="116.84" x2="33.02" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BITLINE" class="0">
<segment>
<wire x1="33.02" y1="101.6" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<label x="60.96" y="101.6" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="4"/>
<wire x1="33.02" y1="73.66" x2="33.02" y2="71.12" width="0.1524" layer="91"/>
<wire x1="33.02" y1="101.6" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="33.02" y1="106.68" x2="33.02" y2="101.6" width="0.1524" layer="91"/>
<junction x="33.02" y="101.6"/>
<pinref part="D5" gate="G$1" pin="2"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="76.2" y1="73.66" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<junction x="33.02" y="73.66"/>
</segment>
</net>
<net name="RECEIVE_ENABLE" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="1"/>
<wire x1="68.58" y1="88.9" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
<wire x1="68.58" y1="83.82" x2="137.16" y2="83.82" width="0.1524" layer="91"/>
<label x="101.6" y="83.82" size="1.778" layer="95"/>
<label x="137.16" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<pinref part="ED" gate="G$1" pin="1"/>
<wire x1="147.32" y1="68.58" x2="147.32" y2="73.66" width="0.1524" layer="91"/>
<wire x1="147.32" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<wire x1="142.24" y1="73.66" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="142.24" y="73.66"/>
<label x="139.7" y="76.2" size="1.778" layer="95"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="132.08" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<wire x1="132.08" y1="71.12" x2="132.08" y2="73.66" width="0.1524" layer="91"/>
<pinref part="RS" gate="G$1" pin="1"/>
<wire x1="132.08" y1="73.66" x2="127" y2="73.66" width="0.1524" layer="91"/>
<junction x="132.08" y="73.66"/>
</segment>
</net>
<net name="CURRENT_SENSOR" class="0">
<segment>
<wire x1="104.14" y1="73.66" x2="104.14" y2="48.26" width="0.1524" layer="91"/>
<wire x1="104.14" y1="48.26" x2="121.92" y2="48.26" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="104.14" y1="73.66" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<label x="91.44" y="73.66" size="1.778" layer="95"/>
<label x="148.844" y="48.26" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="RS" gate="G$1" pin="2"/>
<wire x1="116.84" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<junction x="104.14" y="73.66"/>
</segment>
</net>
<net name="STAGE4" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="1"/>
<pinref part="D4" gate="G$1" pin="2"/>
<wire x1="33.02" y1="60.96" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<pinref part="CP4" gate="G$1" pin="1"/>
<wire x1="33.02" y1="58.42" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<junction x="33.02" y="58.42"/>
<label x="40.64" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="STAGE3" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="1"/>
<pinref part="D3" gate="G$1" pin="2"/>
<wire x1="33.02" y1="45.72" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<pinref part="CP3" gate="G$1" pin="1"/>
<wire x1="33.02" y1="43.18" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
<wire x1="53.34" y1="43.18" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<junction x="33.02" y="43.18"/>
<label x="40.64" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="STAGE1" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="2"/>
<wire x1="33.02" y1="15.24" x2="33.02" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CP1" gate="G$1" pin="1"/>
<wire x1="33.02" y1="12.7" x2="33.02" y2="10.16" width="0.1524" layer="91"/>
<wire x1="33.02" y1="12.7" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<junction x="33.02" y="12.7"/>
<label x="40.64" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="STAGE2" class="0">
<segment>
<pinref part="CP2" gate="G$1" pin="1"/>
<wire x1="33.02" y1="27.94" x2="53.34" y2="27.94" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="2"/>
<wire x1="33.02" y1="30.48" x2="33.02" y2="27.94" width="0.1524" layer="91"/>
<wire x1="33.02" y1="27.94" x2="33.02" y2="25.4" width="0.1524" layer="91"/>
<junction x="33.02" y="27.94"/>
<label x="40.64" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="NETCS1_2" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="3"/>
<pinref part="CS1" gate="G$1" pin="2"/>
<wire x1="25.4" y1="5.08" x2="27.94" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NETCS2_2" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="3"/>
<pinref part="CS2" gate="G$1" pin="2"/>
<wire x1="25.4" y1="20.32" x2="27.94" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NETCS3_2" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="3"/>
<pinref part="CS3" gate="G$1" pin="2"/>
<wire x1="25.4" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NETCS4_2" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="3"/>
<pinref part="CS4" gate="G$1" pin="2"/>
<wire x1="25.4" y1="50.8" x2="27.94" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NETCS5_2" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="3"/>
<pinref part="CS5" gate="G$1" pin="2"/>
<wire x1="25.4" y1="66.04" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FEEDER" class="0">
<segment>
<pinref part="CS5" gate="G$1" pin="1"/>
<wire x1="17.78" y1="66.04" x2="17.78" y2="50.8" width="0.1524" layer="91"/>
<pinref part="CS1" gate="G$1" pin="1"/>
<wire x1="17.78" y1="50.8" x2="17.78" y2="35.56" width="0.1524" layer="91"/>
<wire x1="17.78" y1="35.56" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<wire x1="17.78" y1="20.32" x2="17.78" y2="5.08" width="0.1524" layer="91"/>
<pinref part="CS2" gate="G$1" pin="1"/>
<junction x="17.78" y="20.32"/>
<pinref part="CS3" gate="G$1" pin="1"/>
<junction x="17.78" y="35.56"/>
<pinref part="CS4" gate="G$1" pin="1"/>
<junction x="17.78" y="50.8"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="17.78" y1="5.08" x2="12.7" y2="5.08" width="0.1524" layer="91"/>
<junction x="17.78" y="5.08"/>
<label x="17.78" y="10.16" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="ANT" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="5.08" x2="-30.48" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="5.08" x2="-30.48" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-2.54" x2="-50.8" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="-2.54" x2="-50.8" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-30.48" y="5.08"/>
<wire x1="-30.48" y1="5.08" x2="-35.56" y2="5.08" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="C"/>
<wire x1="-30.48" y1="-7.62" x2="-30.48" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-30.48" y="-2.54"/>
<pinref part="CV1" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="5.08" width="0.1524" layer="91"/>
<junction x="-7.62" y="5.08"/>
<label x="-35.56" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="Q4" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="SMA1" gate="G$1" pin="P1"/>
<wire x1="-50.8" y1="40.64" x2="-40.64" y2="40.64" width="0.1524" layer="91"/>
<label x="-40.64" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="TRANSMIT_RFID" class="0">
<segment>
<wire x1="-45.72" y1="-17.78" x2="-40.64" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-17.78" x2="-40.64" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-27.94" x2="-5.08" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-27.94" x2="-5.08" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-12.7" x2="25.4" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-12.7" x2="38.1" y2="-12.7" width="0.1524" layer="91"/>
<label x="7.62" y="-12.7" size="1.778" layer="95"/>
<label x="38.1" y="-12.7" size="1.778" layer="95" xref="yes"/>
<pinref part="PP1" gate="G$1" pin="1"/>
<wire x1="25.4" y1="-17.78" x2="25.4" y2="-12.7" width="0.1524" layer="91"/>
<junction x="25.4" y="-12.7"/>
<pinref part="Q4" gate="G$1" pin="1"/>
<pinref part="Q4" gate="G$1" pin="4"/>
<wire x1="-50.8" y1="-17.78" x2="-55.88" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-17.78" x2="-55.88" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-30.48" x2="-40.64" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="-30.48" x2="-40.64" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-40.64" y="-27.94"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="-12.7" x2="-10.16" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-5.08" y="-12.7"/>
</segment>
</net>
<net name="RECEIVE" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="B"/>
<wire x1="144.78" y1="104.14" x2="162.56" y2="104.14" width="0.1524" layer="91"/>
<label x="175.768" y="102.616" size="1.778" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="CONTROL" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="B"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="-12.7" x2="-20.32" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="A"/>
<wire x1="99.06" y1="104.14" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="2"/>
<wire x1="88.9" y1="96.52" x2="88.9" y2="93.98" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="3"/>
<wire x1="88.9" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="104.14" y="-63.5" size="1.016" layer="94">Converted to EAGLE by: Josiah Hester
Contact jhester@clemson.edu if errors are found.
</text>
<text x="103.124" y="-53.34" size="1.778" layer="97">Designed by: Hong Zhang &amp; Jeremy Gummerson
Copyright 2011 UMass Amherst</text>
<text x="157.48" y="-45.72" size="2.1844" layer="94">Moo 1.2.1 Digital</text>
<text x="142.24" y="-5.08" size="2.54" layer="97" font="vector" ratio="15">UCBI SPI Mode</text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="-2.54" y="83.82"/>
<instance part="U5" gate="G$1" x="-25.4" y="-33.02"/>
<instance part="U4" gate="G$1" x="12.7" y="-38.1"/>
<instance part="U7" gate="G$1" x="157.48" y="-22.86"/>
<instance part="Y1" gate="G$1" x="30.48" y="38.1"/>
<instance part="FRAME2" gate="G$1" x="-59.436" y="-66.548"/>
<instance part="C20" gate="G$1" x="119.38" y="-17.78"/>
<instance part="GND7" gate="1" x="119.38" y="-38.1"/>
<instance part="R10" gate="G$1" x="175.26" y="25.4" rot="R90"/>
<instance part="R11" gate="G$1" x="185.42" y="25.4" rot="R90"/>
<instance part="P1" gate="G$1" x="152.4" y="91.44"/>
<instance part="GND8" gate="1" x="177.8" y="88.9" rot="R90"/>
<instance part="P2" gate="G$1" x="55.88" y="-38.1"/>
<instance part="P3" gate="G$1" x="-25.4" y="55.88" rot="R270"/>
<instance part="P4" gate="G$1" x="165.1" y="53.34"/>
<instance part="GND9" gate="1" x="-2.54" y="43.18" rot="R90"/>
<instance part="GND18" gate="1" x="-10.16" y="-27.94"/>
<instance part="GND19" gate="1" x="30.48" y="-53.34"/>
<instance part="GND20" gate="1" x="-2.54" y="-53.34"/>
<instance part="C14" gate="G$1" x="20.32" y="76.2" rot="R180"/>
<instance part="R5" gate="G$1" x="-30.48" y="86.36" rot="R270"/>
<instance part="R4" gate="G$1" x="-43.18" y="73.66"/>
<instance part="C7" gate="G$1" x="-30.48" y="68.58"/>
<instance part="GND21" gate="1" x="20.32" y="68.58"/>
<instance part="GND22" gate="1" x="-17.78" y="76.2"/>
<instance part="GND23" gate="1" x="-30.48" y="60.96"/>
<instance part="MCU1" gate="G$1" x="83.82" y="40.64"/>
<instance part="C6" gate="G$1" x="20.32" y="40.64" rot="R180"/>
<instance part="C5" gate="G$1" x="10.16" y="40.64" rot="R180"/>
<instance part="GND24" gate="1" x="10.16" y="33.02"/>
<instance part="GND25" gate="1" x="20.32" y="33.02"/>
<instance part="GND26" gate="1" x="40.64" y="35.56" rot="R270"/>
<instance part="GND27" gate="1" x="81.28" y="35.56"/>
<instance part="R1" gate="G$1" x="139.7" y="27.94"/>
<instance part="GND28" gate="1" x="91.44" y="88.9" rot="R180"/>
<instance part="R6" gate="G$1" x="78.74" y="88.9" rot="R90"/>
<instance part="GND29" gate="1" x="68.58" y="83.82" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="RST" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="RST/HOLD"/>
<wire x1="172.72" y1="-20.32" x2="187.96" y2="-20.32" width="0.1524" layer="91"/>
<label x="180.34" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="G$1" pin="4"/>
<wire x1="162.56" y1="81.28" x2="175.26" y2="81.28" width="0.1524" layer="91"/>
<label x="165.1" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="!RST"/>
<wire x1="78.74" y1="73.66" x2="78.74" y2="83.82" width="0.1524" layer="91"/>
<label x="78.74" y="76.2" size="1.778" layer="95" rot="R90"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="FLASH_SCK" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="SCK"/>
<wire x1="172.72" y1="-25.4" x2="187.96" y2="-25.4" width="0.1524" layer="91"/>
<label x="180.34" y="-25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P5.3"/>
<wire x1="116.84" y1="55.88" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
<label x="119.38" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="FLASH_SIMO" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="SI"/>
<wire x1="172.72" y1="-30.48" x2="187.96" y2="-30.48" width="0.1524" layer="91"/>
<label x="180.34" y="-30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P5.1"/>
<wire x1="116.84" y1="50.8" x2="132.08" y2="50.8" width="0.1524" layer="91"/>
<label x="119.38" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="FLASH_SOMI" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="SO"/>
<wire x1="142.24" y1="-20.32" x2="127" y2="-20.32" width="0.1524" layer="91"/>
<label x="127" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P5.2"/>
<wire x1="116.84" y1="53.34" x2="132.08" y2="53.34" width="0.1524" layer="91"/>
<label x="119.38" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="VREG" class="0">
<segment>
<wire x1="180.34" y1="-15.24" x2="180.34" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VDD"/>
<wire x1="180.34" y1="-7.62" x2="180.34" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-15.24" x2="180.34" y2="-15.24" width="0.1524" layer="91"/>
<label x="177.8" y="-2.54" size="1.778" layer="95"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="180.34" y1="-7.62" x2="119.38" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-7.62" x2="119.38" y2="-12.7" width="0.1524" layer="91"/>
<junction x="180.34" y="-7.62"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="175.26" y1="30.48" x2="175.26" y2="33.02" width="0.1524" layer="91"/>
<label x="172.72" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="185.42" y1="30.48" x2="185.42" y2="33.02" width="0.1524" layer="91"/>
<label x="182.88" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="G$1" pin="10"/>
<wire x1="162.56" y1="96.52" x2="167.64" y2="96.52" width="0.1524" layer="91"/>
<wire x1="167.64" y1="96.52" x2="167.64" y2="101.6" width="0.1524" layer="91"/>
<label x="170.18" y="104.14" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="P3" gate="G$1" pin="13"/>
<wire x1="-12.7" y1="45.72" x2="-12.7" y2="30.48" width="0.1524" layer="91"/>
<label x="-12.7" y="30.48" size="1.778" layer="95"/>
<wire x1="-12.7" y1="30.48" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VCCB"/>
<wire x1="27.94" y1="-35.56" x2="33.02" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-35.56" x2="33.02" y2="-30.48" width="0.1524" layer="91"/>
<label x="30.48" y="-30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="DVCCL"/>
<wire x1="48.26" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
<label x="38.1" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="78.74" y1="93.98" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<label x="76.2" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="AVCC"/>
<wire x1="63.5" y1="73.66" x2="63.5" y2="83.82" width="0.1524" layer="91"/>
<label x="63.5" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VOUT"/>
<wire x1="10.16" y1="81.28" x2="20.32" y2="81.28" width="0.1524" layer="91"/>
<wire x1="20.32" y1="81.28" x2="20.32" y2="83.82" width="0.1524" layer="91"/>
<label x="20.32" y="81.28" size="1.778" layer="95"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="20.32" y1="78.74" x2="20.32" y2="81.28" width="0.1524" layer="91"/>
<junction x="20.32" y="81.28"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="VSS"/>
<wire x1="142.24" y1="-30.48" x2="119.38" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="WP#"/>
<wire x1="142.24" y1="-25.4" x2="119.38" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-25.4" x2="119.38" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-30.48" x2="119.38" y2="-35.56" width="0.1524" layer="91"/>
<junction x="119.38" y="-30.48"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="119.38" y1="-20.32" x2="119.38" y2="-25.4" width="0.1524" layer="91"/>
<junction x="119.38" y="-25.4"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-10.16" y1="43.18" x2="-5.08" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="P3" gate="G$1" pin="14"/>
<wire x1="-10.16" y1="43.18" x2="-10.16" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="30.48" y1="-45.72" x2="30.48" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="OE"/>
<wire x1="27.94" y1="-45.72" x2="30.48" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="-2.54" y1="-45.72" x2="-2.54" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="0" y1="-45.72" x2="-2.54" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="-17.78" y1="81.28" x2="-17.78" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="-15.24" y1="81.28" x2="-17.78" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="45.72" y1="35.56" x2="43.18" y2="35.56" width="0.1524" layer="91"/>
<wire x1="45.72" y1="33.02" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="VREF-/VEREF-"/>
<wire x1="45.72" y1="33.02" x2="48.26" y2="33.02" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="VEREF+/DAC0"/>
<wire x1="48.26" y1="35.56" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
<junction x="45.72" y="35.56"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="81.28" y1="40.64" x2="81.28" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="68.58" y1="78.74" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="66.04" y1="78.74" x2="68.58" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="MCU1" gate="G$1" pin="DVSS1"/>
<wire x1="66.04" y1="78.74" x2="66.04" y2="73.66" width="0.1524" layer="91"/>
<pinref part="MCU1" gate="G$1" pin="AVSS"/>
<wire x1="68.58" y1="78.74" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<junction x="68.58" y="78.74"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="MCU1" gate="G$1" pin="XT2IN"/>
<wire x1="91.44" y1="86.36" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="-"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P1" gate="G$1" pin="7"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="162.56" y1="88.9" x2="175.26" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="-12.7" y1="-25.4" x2="-10.16" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL_B" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="185.42" y1="20.32" x2="185.42" y2="10.16" width="0.1524" layer="91"/>
<label x="185.42" y="10.16" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="P2" gate="G$1" pin="3"/>
<wire x1="66.04" y1="-50.8" x2="81.28" y2="-50.8" width="0.1524" layer="91"/>
<label x="71.12" y="-50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.2"/>
<wire x1="96.52" y1="5.08" x2="96.52" y2="-7.62" width="0.1524" layer="91"/>
<label x="96.52" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SDA_B" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="175.26" y1="20.32" x2="175.26" y2="10.16" width="0.1524" layer="91"/>
<label x="175.26" y="10.16" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="P2" gate="G$1" pin="2"/>
<wire x1="66.04" y1="-53.34" x2="81.28" y2="-53.34" width="0.1524" layer="91"/>
<label x="71.12" y="-53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.1"/>
<wire x1="93.98" y1="5.08" x2="93.98" y2="-7.62" width="0.1524" layer="91"/>
<label x="93.98" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="9"/>
<wire x1="162.56" y1="93.98" x2="175.26" y2="93.98" width="0.1524" layer="91"/>
<label x="165.1" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="TCK"/>
<wire x1="81.28" y1="73.66" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<label x="81.28" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="8"/>
<wire x1="162.56" y1="91.44" x2="175.26" y2="91.44" width="0.1524" layer="91"/>
<label x="165.1" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="TDO/TDI"/>
<wire x1="88.9" y1="73.66" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<label x="88.9" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="6"/>
<wire x1="162.56" y1="86.36" x2="175.26" y2="86.36" width="0.1524" layer="91"/>
<label x="165.1" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="TMS"/>
<wire x1="83.82" y1="73.66" x2="83.82" y2="83.82" width="0.1524" layer="91"/>
<label x="83.82" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="5"/>
<wire x1="162.56" y1="83.82" x2="175.26" y2="83.82" width="0.1524" layer="91"/>
<label x="165.1" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="TDI/TCLK"/>
<wire x1="86.36" y1="73.66" x2="86.36" y2="83.82" width="0.1524" layer="91"/>
<label x="86.36" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SVSOUT" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="3"/>
<wire x1="162.56" y1="78.74" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
<label x="165.1" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P5.7"/>
<wire x1="96.52" y1="73.66" x2="96.52" y2="83.82" width="0.1524" layer="91"/>
<label x="96.52" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SMCLK" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="2"/>
<wire x1="162.56" y1="76.2" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<label x="165.1" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P5.5"/>
<wire x1="101.6" y1="73.66" x2="101.6" y2="83.82" width="0.1524" layer="91"/>
<label x="101.6" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="MCLK" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="1"/>
<wire x1="162.56" y1="73.66" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<label x="165.1" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P5.4"/>
<wire x1="116.84" y1="58.42" x2="132.08" y2="58.42" width="0.1524" layer="91"/>
<label x="119.38" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAPSENSELINE" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="14"/>
<wire x1="66.04" y1="-22.86" x2="86.36" y2="-22.86" width="0.1524" layer="91"/>
<label x="115.316" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P47" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="13"/>
<wire x1="66.04" y1="-25.4" x2="81.28" y2="-25.4" width="0.1524" layer="91"/>
<label x="71.12" y="-25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.7"/>
<wire x1="116.84" y1="45.72" x2="132.08" y2="45.72" width="0.1524" layer="91"/>
<label x="119.38" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="P46" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="12"/>
<wire x1="66.04" y1="-27.94" x2="81.28" y2="-27.94" width="0.1524" layer="91"/>
<label x="71.12" y="-27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.6"/>
<wire x1="116.84" y1="43.18" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<label x="119.38" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="P45" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="11"/>
<wire x1="66.04" y1="-30.48" x2="81.28" y2="-30.48" width="0.1524" layer="91"/>
<label x="71.12" y="-30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.5"/>
<wire x1="116.84" y1="40.64" x2="132.08" y2="40.64" width="0.1524" layer="91"/>
<label x="119.38" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="P44" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="10"/>
<wire x1="66.04" y1="-33.02" x2="81.28" y2="-33.02" width="0.1524" layer="91"/>
<label x="71.12" y="-33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.4"/>
<wire x1="116.84" y1="38.1" x2="132.08" y2="38.1" width="0.1524" layer="91"/>
<label x="119.38" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="P40" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="9"/>
<wire x1="66.04" y1="-35.56" x2="81.28" y2="-35.56" width="0.1524" layer="91"/>
<label x="71.12" y="-35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.0"/>
<wire x1="116.84" y1="27.94" x2="134.62" y2="27.94" width="0.1524" layer="91"/>
<label x="119.38" y="27.94" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="P37" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="8"/>
<wire x1="66.04" y1="-38.1" x2="81.28" y2="-38.1" width="0.1524" layer="91"/>
<label x="71.12" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.7"/>
<wire x1="116.84" y1="25.4" x2="132.08" y2="25.4" width="0.1524" layer="91"/>
<label x="119.38" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="XL_CS" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.6"/>
<wire x1="116.84" y1="22.86" x2="132.08" y2="22.86" width="0.1524" layer="91"/>
<label x="119.38" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="G$1" pin="7"/>
<wire x1="66.04" y1="-40.64" x2="81.28" y2="-40.64" width="0.1524" layer="91"/>
<label x="71.12" y="-40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="XL_MISO" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.5"/>
<wire x1="116.84" y1="20.32" x2="132.08" y2="20.32" width="0.1524" layer="91"/>
<label x="119.38" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="G$1" pin="6"/>
<wire x1="66.04" y1="-43.18" x2="81.28" y2="-43.18" width="0.1524" layer="91"/>
<label x="71.12" y="-43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="XL_MOSI" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.4"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="-7.62" width="0.1524" layer="91"/>
<label x="101.6" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="P2" gate="G$1" pin="5"/>
<wire x1="66.04" y1="-45.72" x2="81.28" y2="-45.72" width="0.1524" layer="91"/>
<label x="71.12" y="-45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLK_B" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="4"/>
<wire x1="66.04" y1="-48.26" x2="81.28" y2="-48.26" width="0.1524" layer="91"/>
<label x="71.12" y="-48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.3"/>
<wire x1="99.06" y1="5.08" x2="99.06" y2="-7.62" width="0.1524" layer="91"/>
<label x="99.06" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="XL_SCLK" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P3.0"/>
<wire x1="91.44" y1="5.08" x2="91.44" y2="-7.62" width="0.1524" layer="91"/>
<label x="91.44" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SCAPCHARGE" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="12"/>
<wire x1="-15.24" y1="45.72" x2="-15.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="27.94" x2="2.54" y2="27.94" width="0.1524" layer="91"/>
<label x="-12.7" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="73.66" x2="-50.8" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="73.66" x2="-50.8" y2="91.44" width="0.1524" layer="91"/>
<label x="-50.8" y="76.2" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="P67_A7" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="11"/>
<wire x1="-17.78" y1="45.72" x2="-17.78" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="25.4" x2="2.54" y2="25.4" width="0.1524" layer="91"/>
<label x="-12.7" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P6.7/A7/DAC1/SVSIN"/>
<wire x1="48.26" y1="45.72" x2="38.1" y2="45.72" width="0.1524" layer="91"/>
<label x="38.1" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="P15" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="8"/>
<wire x1="-25.4" y1="45.72" x2="-25.4" y2="38.1" width="0.1524" layer="91"/>
<label x="-25.4" y="38.1" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.5/TA0"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="-7.62" width="0.1524" layer="91"/>
<label x="63.5" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="P16" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="7"/>
<wire x1="-27.94" y1="45.72" x2="-27.94" y2="38.1" width="0.1524" layer="91"/>
<label x="-27.94" y="38.1" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.6/TA1"/>
<wire x1="66.04" y1="5.08" x2="66.04" y2="-7.62" width="0.1524" layer="91"/>
<label x="66.04" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="P17" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="6"/>
<wire x1="-30.48" y1="45.72" x2="-30.48" y2="38.1" width="0.1524" layer="91"/>
<label x="-30.48" y="38.1" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.7/TA2"/>
<wire x1="68.58" y1="5.08" x2="68.58" y2="-7.62" width="0.1524" layer="91"/>
<label x="68.58" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SV_IN" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="5"/>
<wire x1="-33.02" y1="45.72" x2="-33.02" y2="38.1" width="0.1524" layer="91"/>
<label x="-33.02" y="38.1" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="B"/>
<wire x1="27.94" y1="-40.64" x2="48.26" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-40.64" x2="48.26" y2="-25.4" width="0.1524" layer="91"/>
<label x="35.56" y="-40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P2.0/ACLK/CA2"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="-7.62" width="0.1524" layer="91"/>
<label x="71.12" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="144.78" y1="27.94" x2="157.48" y2="27.94" width="0.1524" layer="91"/>
<label x="149.86" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="CURRENT_SENSOR" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="45.72" x2="-43.18" y2="40.64" width="0.1524" layer="91"/>
<label x="-43.18" y="40.64" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<wire x1="-40.64" y1="30.48" x2="-33.02" y2="30.48" width="0.1524" layer="91"/>
<label x="-38.1" y="30.48" size="1.778" layer="95"/>
<pinref part="P3" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="30.48" x2="-40.64" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-2.54" y1="-35.56" x2="-2.54" y2="-33.02" width="0.1524" layer="91"/>
<label x="-5.08" y="-17.78" size="1.778" layer="95"/>
<pinref part="U4" gate="G$1" pin="VCCA"/>
<wire x1="-2.54" y1="-33.02" x2="-2.54" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="0" y1="-35.56" x2="-2.54" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="IN"/>
<wire x1="-2.54" y1="-33.02" x2="-12.7" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-2.54" y="-33.02"/>
</segment>
<segment>
<wire x1="22.86" y1="86.36" x2="22.86" y2="96.52" width="0.1524" layer="91"/>
<wire x1="22.86" y1="96.52" x2="-12.7" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="96.52" x2="-17.78" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="86.36" x2="-17.78" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="96.52" x2="-30.48" y2="96.52" width="0.1524" layer="91"/>
<junction x="-17.78" y="96.52"/>
<wire x1="-12.7" y1="96.52" x2="-12.7" y2="101.6" width="0.1524" layer="91"/>
<junction x="-12.7" y="96.52"/>
<label x="-15.24" y="101.6" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="VIN"/>
<wire x1="22.86" y1="86.36" x2="10.16" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="CE"/>
<wire x1="-15.24" y1="86.36" x2="-17.78" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-30.48" y1="96.52" x2="-30.48" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="P63_A3" class="0">
<segment>
<pinref part="P4" gate="G$1" pin="1"/>
<wire x1="170.18" y1="50.8" x2="182.88" y2="50.8" width="0.1524" layer="91"/>
<label x="175.26" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P6.3/A3"/>
<label x="38.1" y="55.88" size="1.778" layer="95"/>
<wire x1="48.26" y1="55.88" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="P65_A5" class="0">
<segment>
<pinref part="P4" gate="G$1" pin="3"/>
<wire x1="170.18" y1="55.88" x2="182.88" y2="55.88" width="0.1524" layer="91"/>
<label x="175.26" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P6.5/A5/DAC1"/>
<wire x1="48.26" y1="50.8" x2="38.1" y2="50.8" width="0.1524" layer="91"/>
<label x="38.1" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="P66_A6" class="0">
<segment>
<pinref part="P4" gate="G$1" pin="4"/>
<wire x1="170.18" y1="58.42" x2="182.88" y2="58.42" width="0.1524" layer="91"/>
<label x="175.26" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P6.6/A6/DAC0"/>
<wire x1="48.26" y1="48.26" x2="38.1" y2="48.26" width="0.1524" layer="91"/>
<label x="38.1" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="SV_OUT" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="A"/>
<pinref part="U5" gate="G$1" pin="OUT"/>
<wire x1="0" y1="-40.64" x2="-12.7" y2="-40.64" width="0.1524" layer="91"/>
<label x="-10.16" y="-40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="MAXCAP" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="+"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="73.66" x2="-30.48" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="73.66" x2="-38.1" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="81.28" x2="-30.48" y2="73.66" width="0.1524" layer="91"/>
<junction x="-30.48" y="73.66"/>
</segment>
</net>
<net name="XIN" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="XIN"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="48.26" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XOUT" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="XOUT"/>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="48.26" y1="38.1" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
<wire x1="35.56" y1="38.1" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP_PWR" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.0/TACLK/CAOUT"/>
<wire x1="48.26" y1="30.48" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<label x="33.02" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="TRANSMIT_RFID" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.1/TA0"/>
<wire x1="48.26" y1="27.94" x2="38.1" y2="27.94" width="0.1524" layer="91"/>
<label x="27.94" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="XLPWR" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.4/SMCLK"/>
<wire x1="48.26" y1="20.32" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<label x="38.1" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="XL_INT1" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P2.1/TAINCLK/CA3"/>
<wire x1="73.66" y1="5.08" x2="73.66" y2="-7.62" width="0.1524" layer="91"/>
<label x="73.66" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="XL_INT2" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P2.2"/>
<wire x1="76.2" y1="5.08" x2="76.2" y2="-7.62" width="0.1524" layer="91"/>
<label x="76.2" y="-7.62" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CAPSENSE" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.1"/>
<wire x1="116.84" y1="30.48" x2="132.08" y2="30.48" width="0.1524" layer="91"/>
<label x="119.38" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED_CTL_1" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.2"/>
<wire x1="116.84" y1="33.02" x2="132.08" y2="33.02" width="0.1524" layer="91"/>
<label x="119.38" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="RECEIVE" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="10"/>
<wire x1="-20.32" y1="45.72" x2="-20.32" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="22.86" x2="-17.78" y2="22.86" width="0.1524" layer="91"/>
<label x="-17.78" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.2/TA1"/>
<wire x1="48.26" y1="25.4" x2="38.1" y2="25.4" width="0.1524" layer="91"/>
<label x="27.94" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="RECEIVE_ENABLE" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="9"/>
<wire x1="-22.86" y1="45.72" x2="-22.86" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="17.78" x2="-20.32" y2="17.78" width="0.1524" layer="91"/>
<label x="-20.32" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="MCU1" gate="G$1" pin="P1.3/TA2"/>
<wire x1="48.26" y1="22.86" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<label x="17.78" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="VREF+" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="VREF+"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="48.26" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="20.32" y1="43.18" x2="10.16" y2="43.18" width="0.1524" layer="91"/>
<junction x="20.32" y="43.18"/>
</segment>
</net>
<net name="P64_A4" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P6.4/A4"/>
<wire x1="48.26" y1="53.34" x2="38.1" y2="53.34" width="0.1524" layer="91"/>
<label x="38.1" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P4" gate="G$1" pin="2"/>
<wire x1="170.18" y1="53.34" x2="182.88" y2="53.34" width="0.1524" layer="91"/>
<label x="175.26" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="FLASH_CE#" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P5.0"/>
<wire x1="116.84" y1="48.26" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
<label x="119.38" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="CE#"/>
<wire x1="142.24" y1="-15.24" x2="127" y2="-15.24" width="0.1524" layer="91"/>
<label x="127" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSENSE_ENABLE" class="0">
<segment>
<pinref part="MCU1" gate="G$1" pin="P4.3"/>
<wire x1="116.84" y1="35.56" x2="132.08" y2="35.56" width="0.1524" layer="91"/>
<label x="119.38" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="101.6" y="-48.26" size="1.016" layer="94">Converted to EAGLE by: Josiah Hester
Contact jhester@clemson.edu if errors are found.
</text>
<text x="100.584" y="-38.1" size="1.778" layer="97">Designed by: Dan Yeager &amp; Alanson Sample
Copyright 2009 Intel Corporation</text>
<text x="154.94" y="-30.48" size="2.1844" layer="94">Moo 1.2.1 Sensors</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="-61.976" y="-51.308"/>
<instance part="R16" gate="G$1" x="-38.1" y="25.4" rot="R90"/>
<instance part="R18" gate="G$1" x="-38.1" y="7.62" rot="R90"/>
<instance part="C11" gate="G$1" x="-22.86" y="7.62"/>
<instance part="GND30" gate="1" x="-22.86" y="0"/>
<instance part="GND31" gate="1" x="-38.1" y="0"/>
<instance part="U6" gate="G$1" x="53.34" y="71.12"/>
<instance part="TMP" gate="G$1" x="129.54" y="99.06"/>
<instance part="XL1" gate="G$1" x="83.82" y="12.7"/>
<instance part="Q5" gate="G$1" x="12.7" y="45.72" rot="R90"/>
<instance part="R19" gate="G$1" x="12.7" y="88.9" rot="R90"/>
<instance part="R17" gate="G$1" x="12.7" y="68.58" rot="R90"/>
<instance part="C10" gate="G$1" x="27.94" y="63.5"/>
<instance part="GND32" gate="1" x="27.94" y="58.42"/>
<instance part="GND33" gate="1" x="38.1" y="60.96"/>
<instance part="GND34" gate="1" x="12.7" y="35.56"/>
<instance part="C13" gate="G$1" x="154.94" y="86.36" rot="R180"/>
<instance part="C12" gate="G$1" x="104.14" y="88.9" rot="R180"/>
<instance part="GND35" gate="1" x="104.14" y="81.28"/>
<instance part="GND36" gate="1" x="154.94" y="78.74"/>
<instance part="GND37" gate="1" x="144.78" y="99.06"/>
<instance part="GND38" gate="1" x="106.68" y="99.06"/>
<instance part="C4" gate="G$1" x="127" y="30.48"/>
<instance part="GND39" gate="1" x="127" y="25.4"/>
<instance part="GND40" gate="1" x="114.3" y="25.4"/>
<instance part="GND41" gate="1" x="109.22" y="17.78" rot="R90"/>
<instance part="GND42" gate="1" x="109.22" y="5.08" rot="R90"/>
<instance part="GND43" gate="1" x="58.42" y="10.16" rot="R270"/>
<instance part="GND44" gate="1" x="58.42" y="0" rot="R270"/>
<instance part="GND45" gate="1" x="66.04" y="25.4"/>
<instance part="C3" gate="G$1" x="114.3" y="33.02"/>
<instance part="Q3" gate="G$1" x="0" y="-35.56" rot="R180"/>
<instance part="R2" gate="G$1" x="-17.78" y="-35.56" rot="R180"/>
<instance part="GND46" gate="1" x="12.7" y="-38.1"/>
<instance part="DA1" gate="G$1" x="-33.02" y="-35.56" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="-22.86" y1="2.54" x2="-22.86" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="38.1" y1="66.04" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="38.1" y1="66.04" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q5" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="12.7" y1="38.1" x2="12.7" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="111.76" y1="104.14" x2="111.76" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="111.76" y1="101.6" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<wire x1="111.76" y1="99.06" x2="111.76" y2="101.6" width="0.1524" layer="91"/>
<junction x="111.76" y="101.6"/>
<pinref part="TMP" gate="G$1" pin="GS2"/>
<wire x1="116.84" y1="104.14" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<pinref part="TMP" gate="G$1" pin="GND"/>
<wire x1="111.76" y1="99.06" x2="116.84" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="144.78" y1="104.14" x2="144.78" y2="101.6" width="0.1524" layer="91"/>
<pinref part="TMP" gate="G$1" pin="GS1"/>
<wire x1="142.24" y1="104.14" x2="144.78" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="104.14" y1="20.32" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="104.14" y1="17.78" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="15.24" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<junction x="104.14" y="17.78"/>
<pinref part="XL1" gate="G$1" pin="GND@2"/>
<wire x1="101.6" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<pinref part="XL1" gate="G$1" pin="GND@1"/>
<wire x1="104.14" y1="15.24" x2="101.6" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="78.74" y1="33.02" x2="66.04" y2="33.02" width="0.1524" layer="91"/>
<wire x1="66.04" y1="33.02" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
<pinref part="XL1" gate="G$1" pin="GND@3"/>
<wire x1="78.74" y1="33.02" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND46" gate="1" pin="GND"/>
<pinref part="Q3" gate="G$1" pin="2"/>
<wire x1="12.7" y1="-35.56" x2="5.08" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="GND35" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="GND36" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="XL1" gate="G$1" pin="RSVRD@1"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="66.04" y1="10.16" x2="60.96" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XL1" gate="G$1" pin="RSVRD@2"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="66.04" y1="0" x2="60.96" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XL1" gate="G$1" pin="RSVRD@3"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="101.6" y1="5.08" x2="106.68" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="-"/>
<pinref part="GND40" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
</net>
<net name="CAPSENSE" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="-38.1" y1="20.32" x2="-38.1" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="12.7" x2="-22.86" y2="12.7" width="0.1524" layer="91"/>
<junction x="-38.1" y="12.7"/>
<wire x1="-22.86" y1="12.7" x2="-2.54" y2="12.7" width="0.1524" layer="91"/>
<junction x="-22.86" y="12.7"/>
<label x="-20.32" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAPSENSELINE" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-38.1" y1="30.48" x2="-38.1" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="33.02" x2="-2.54" y2="33.02" width="0.1524" layer="91"/>
<label x="-20.32" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="NETQ5_3" class="0">
<segment>
<pinref part="Q5" gate="G$1" pin="3"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="12.7" y1="50.8" x2="12.7" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VSENSE_ENABLE" class="0">
<segment>
<wire x1="71.12" y1="66.04" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="71.12" y1="53.34" x2="71.12" y2="45.72" width="0.1524" layer="91"/>
<wire x1="71.12" y1="45.72" x2="17.78" y2="45.72" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="1"/>
<wire x1="71.12" y1="53.34" x2="78.74" y2="53.34" width="0.1524" layer="91"/>
<junction x="71.12" y="53.34"/>
<label x="78.74" y="53.34" size="1.778" layer="95" xref="yes"/>
<pinref part="U6" gate="G$1" pin="IN"/>
<wire x1="66.04" y1="66.04" x2="71.12" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<wire x1="71.12" y1="76.2" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<label x="68.58" y="81.28" size="1.778" layer="95"/>
<pinref part="U6" gate="G$1" pin="VCC"/>
<wire x1="71.12" y1="76.2" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DA1" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="-35.56" x2="-48.26" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="-35.56" x2="-48.26" y2="-25.4" width="0.1524" layer="91"/>
<label x="-50.8" y="-25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="12.7" y1="93.98" x2="12.7" y2="101.6" width="0.1524" layer="91"/>
<label x="10.16" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="P63_A3" class="0">
<segment>
<pinref part="TMP" gate="G$1" pin="OUT"/>
<wire x1="116.84" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<label x="93.98" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="104.14" y1="93.98" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<wire x1="104.14" y1="91.44" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<junction x="104.14" y="93.98"/>
</segment>
</net>
<net name="TEMP_PWR" class="0">
<segment>
<pinref part="TMP" gate="G$1" pin="VCC"/>
<wire x1="142.24" y1="93.98" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
<label x="165.1" y="93.98" size="1.778" layer="95" xref="yes"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="154.94" y1="93.98" x2="165.1" y2="93.98" width="0.1524" layer="91"/>
<wire x1="154.94" y1="88.9" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
<junction x="154.94" y="93.98"/>
</segment>
</net>
<net name="XLPWR" class="0">
<segment>
<wire x1="58.42" y1="20.32" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<label x="139.7" y="38.1" size="1.778" layer="95" xref="yes"/>
<pinref part="XL1" gate="G$1" pin="VDDI/O"/>
<wire x1="88.9" y1="38.1" x2="114.3" y2="38.1" width="0.1524" layer="91"/>
<wire x1="114.3" y1="38.1" x2="127" y2="38.1" width="0.1524" layer="91"/>
<wire x1="127" y1="38.1" x2="139.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="20.32" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
<pinref part="XL1" gate="G$1" pin="VS"/>
<wire x1="88.9" y1="27.94" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<junction x="88.9" y="38.1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="127" y1="35.56" x2="127" y2="38.1" width="0.1524" layer="91"/>
<junction x="127" y="38.1"/>
<pinref part="C3" gate="G$1" pin="+"/>
<wire x1="114.3" y1="35.56" x2="114.3" y2="38.1" width="0.1524" layer="91"/>
<junction x="114.3" y="38.1"/>
</segment>
</net>
<net name="LED_CTL_1" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="1"/>
<wire x1="0" y1="-30.48" x2="0" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="0" y1="-17.78" x2="-12.7" y2="-17.78" width="0.1524" layer="91"/>
<label x="-12.7" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="DA1" gate="G$1" pin="C"/>
<wire x1="-22.86" y1="-35.56" x2="-27.94" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="Q3" gate="G$1" pin="3"/>
<wire x1="-12.7" y1="-35.56" x2="-5.08" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="P64_A4" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="COM"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="40.64" y1="71.12" x2="27.94" y2="71.12" width="0.1524" layer="91"/>
<wire x1="27.94" y1="71.12" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="27.94" y1="71.12" x2="22.86" y2="71.12" width="0.1524" layer="91"/>
<junction x="27.94" y="71.12"/>
<label x="22.86" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSENSE" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="NO"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="40.64" y1="76.2" x2="12.7" y2="76.2" width="0.1524" layer="91"/>
<wire x1="12.7" y1="76.2" x2="12.7" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="12.7" y1="76.2" x2="12.7" y2="83.82" width="0.1524" layer="91"/>
<junction x="12.7" y="76.2"/>
<label x="22.86" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="XL_INT1" class="0">
<segment>
<pinref part="XL1" gate="G$1" pin="INT1"/>
<wire x1="101.6" y1="10.16" x2="114.3" y2="10.16" width="0.1524" layer="91"/>
<label x="114.3" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="XL_INT2" class="0">
<segment>
<pinref part="XL1" gate="G$1" pin="INT2"/>
<wire x1="101.6" y1="0" x2="114.3" y2="0" width="0.1524" layer="91"/>
<label x="132.08" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="XL_CS" class="0">
<segment>
<pinref part="XL1" gate="G$1" pin="CS"/>
<wire x1="88.9" y1="-7.62" x2="88.9" y2="-17.78" width="0.1524" layer="91"/>
<label x="88.9" y="-17.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="XL_MISO" class="0">
<segment>
<pinref part="XL1" gate="G$1" pin="MISO"/>
<wire x1="83.82" y1="-7.62" x2="83.82" y2="-17.78" width="0.1524" layer="91"/>
<label x="83.82" y="-37.338" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="XL_MOSI" class="0">
<segment>
<pinref part="XL1" gate="G$1" pin="MOSI"/>
<wire x1="78.74" y1="-7.62" x2="78.74" y2="-17.78" width="0.1524" layer="91"/>
<label x="78.74" y="-17.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="XL_SCLK" class="0">
<segment>
<pinref part="XL1" gate="G$1" pin="SCLK"/>
<wire x1="66.04" y1="5.08" x2="63.5" y2="5.08" width="0.1524" layer="91"/>
<label x="63.5" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
