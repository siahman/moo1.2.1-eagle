# Umich Moo 1.2.1 EAGLE Port

This is the EAGLE port of the Moo 1.2.1 schematic and board layout from Altium.

This is incomplete. If any errors are found, please contact: jhester@clemson.edu

Board layout fits with OSH Park specs and[is uploaded to OSH Park and can be ordered.](http://oshpark.com/shared_projects/bzrTRBAV)
![Front](https://bytebucket.org/siahman/moo1.2.1-eagle/raw/87690647010daf7c067d97f8406e79816336c713/front.png)
![Back](https://bytebucket.org/siahman/moo1.2.1-eagle/raw/87690647010daf7c067d97f8406e79816336c713/back.png)